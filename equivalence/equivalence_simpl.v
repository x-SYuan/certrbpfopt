From compcert Require Import Memory Memtype Integers Values Ctypes AST.
From Coq Require Import ZArith Lia List.

From bpf.comm Require Import Flag rBPFValues Regs BinrBPF MemRegion rBPFAST rBPFMemType ListAsArray.
From bpf.comm Require Import State Monad rBPFMonadOp.
From bpf.model Require Import Syntax Decode Semantics SemanticsSimpl.
Import ListNotations.

Lemma check_one_memory_region_eq:
  forall p addr chunk m st1,
    Semantics.check_one_memory_region m p addr chunk st1 =
      Some (check_one_memory_region m p addr chunk, st1).
Proof.
  unfold Semantics.check_one_memory_region, check_one_memory_region, bindM, returnM; intros.
  unfold get_start_addr, get_block_size, get_block_perm, get_sub, get_add, returnM.
  match goal with
  | |- (if ?X then _ else _) _ = _ =>
    destruct X; f_equal
  end.
Qed.

Lemma check_all_memory_regions_simpl_equiv:
  forall n p addr chunk st1 st2 ptr cache_id opt_flag
  (Hcheck: Semantics.check_all_memory_regions n cache_id p chunk addr opt_flag st1 =
         Some (ptr, st2)),
    check_all_memory_regions (pc_loc st1) n cache_id (mrs_num st1) 
      (cache st1) p chunk addr (bpf_mrs st1) (bpf_m st1) opt_flag = Some (ptr, cache st2).
Proof.
  induction n; simpl; unfold bindM, returnM; intros.
  { inversion Hcheck; subst; f_equal. }

  unfold rBPFMonadOp.get_mem_region in Hcheck.
  unfold get_mem_region.
  destruct ((_ <? _)%nat); [| inversion Hcheck].
  destruct nth_error; [| inversion Hcheck].

  destruct opt_flag.
  {
    match goal with
    | H: (if ?X then _ else _) _ = _ |- _ =>
      destruct X
    end.
    - eapply IHn; eauto.
    - rewrite check_one_memory_region_eq in Hcheck.
      unfold cmp_ptr32_nullM, State.eval_mem in Hcheck.
      destruct cmp_ptr32_null as [check_ptr| ]; [| inversion Hcheck].
      destruct check_ptr.
      + eapply IHn; eauto.
      + unfold upd_cache, State.upd_cache in Hcheck.
        destruct ListNat.assign as [cache1| ]; inversion Hcheck; subst; simpl; f_equal.
  }

  rewrite check_one_memory_region_eq in Hcheck.
  unfold cmp_ptr32_nullM, State.eval_mem in Hcheck.
  destruct cmp_ptr32_null as [check_ptr| ]; [| inversion Hcheck].
  destruct check_ptr.
  + eapply IHn; eauto.
  + inversion Hcheck; subst; simpl; f_equal.
Qed.

Lemma check_mem_simpl_equiv:
  forall opt_flag p addr chunk st1 st2 ptr
  (Hcheck: Semantics.check_mem p chunk addr opt_flag st1 = Some (ptr, st2)),
    check_mem (pc_loc st1) (cache st1) p chunk addr opt_flag
      (mrs_num st1) (bpf_mrs st1) (bpf_m st1) = Some (ptr, cache st2).
Proof.
  unfold Semantics.check_mem, check_mem, bindM, returnM; intros.
  destruct opt_flag.
  - unfold eval_cache, State.eval_cache, ListNat.index in Hcheck.
    destruct nth_error as [ cache_id | ]; [| inversion Hcheck].
    destruct ((0 =? _)%nat).
    + unfold eval_mrs_num in Hcheck.
      eapply check_all_memory_regions_simpl_equiv; eauto.
    + unfold rBPFMonadOp.get_mem_region in Hcheck.
      unfold get_mem_region.
      destruct ((_ <? _)%nat); [| inversion Hcheck].
      destruct nth_error as [ cur_mr |]; [| inversion Hcheck].
      rewrite check_one_memory_region_eq in Hcheck.
      unfold cmp_ptr32_nullM, State.eval_mem in Hcheck.
      destruct cmp_ptr32_null as [check_ptr| ]; [| inversion Hcheck].
      destruct check_ptr.
      * unfold eval_mrs_num in Hcheck.
        eapply check_all_memory_regions_simpl_equiv; eauto.
      * inversion Hcheck; subst; simpl; f_equal.
  - unfold eval_mrs_num, eval_mem_num in Hcheck.
    eapply check_all_memory_regions_simpl_equiv; eauto.
Qed.

Lemma step_alu_binary_operation_simpl_equiv:
  forall a b r s st1 vt st2
  (Hflag_eq: flag st1 = BPF_OK)
  (Haux : Semantics.step_alu_binary_operation a b r s st1 = Some (vt, st2)),
    match step_alu_binary_operation a b r s (regs_st st1) with
    | Some (rs', f) => Some (pc_loc st1, rs', bpf_m st1, f, cache st1)
    | None => None
    end = Some (pc_loc st2, regs_st st2, bpf_m st2, flag st2, cache st2).
Proof.
  intros.
  unfold Semantics.step_alu_binary_operation, bindM, returnM in Haux.
  unfold step_alu_binary_operation.
  destruct a.
  + unfold Semantics.eval_src32, Semantics.eval_reg32, bindM, returnM in Haux; simpl in Haux.
    unfold eval_src32, eval_reg32.
    destruct s.
    {
      destruct b.
      - unfold upd_reg, bindM, returnM in Haux; simpl in Haux.
        destruct Val.longofintu as [| vi | vl | vf | vs | vp]; inversion Haux.
        subst.
        unfold State.upd_reg; simpl.
        rewrite Hflag_eq.
        f_equal.
      - unfold upd_reg, bindM, returnM in Haux; simpl in Haux.
        destruct Val.longofintu as [| vi | vl | vf | vs | vp]; inversion Haux.
        subst.
        unfold State.upd_reg; simpl.
        rewrite Hflag_eq.
        f_equal.
      - unfold upd_reg, bindM, returnM in Haux; simpl in Haux.
        destruct Val.longofintu as [| vi | vl | vf | vs | vp]; inversion Haux.
        subst.
        unfold State.upd_reg; simpl.
        rewrite Hflag_eq.
        f_equal.
      - unfold errorM.
        destruct comp_ne.
        + destruct Val.divu; [| inversion Haux].
          unfold upd_reg, bindM, returnM in Haux; simpl in Haux.
          destruct Val.longofintu as [| vi | vl | vf | vs | vp]; inversion Haux.
          subst.
          unfold State.upd_reg; simpl.
          rewrite Hflag_eq.
          f_equal.
        + unfold upd_flag, State.upd_flag, bindM, returnM in Haux; simpl in Haux.
          inversion Haux; subst; simpl.
          f_equal.
      - unfold upd_reg, bindM, returnM in Haux; simpl in Haux.
        destruct Val.longofintu as [| vi | vl | vf | vs | vp]; inversion Haux.
        subst.
        unfold State.upd_reg; simpl.
        rewrite Hflag_eq.
        f_equal.
      - unfold upd_reg, bindM, returnM in Haux; simpl in Haux.
        destruct Val.longofintu as [| vi | vl | vf | vs | vp]; inversion Haux.
        subst.
        unfold State.upd_reg; simpl.
        rewrite Hflag_eq.
        f_equal.
      - destruct compu_lt.
        + unfold upd_reg, bindM, returnM in Haux; simpl in Haux.
          destruct Val.longofintu as [| vi | vl | vf | vs | vp]; inversion Haux.
          subst.
          unfold State.upd_reg; simpl.
          rewrite Hflag_eq.
          f_equal.
        + unfold upd_flag, State.upd_flag, bindM, returnM in Haux; simpl in Haux.
          inversion Haux; subst; simpl.
          f_equal.
      - destruct compu_lt.
        + unfold upd_reg, bindM, returnM in Haux; simpl in Haux.
          destruct Val.longofintu as [| vi | vl | vf | vs | vp]; inversion Haux.
          subst.
          unfold State.upd_reg; simpl.
          rewrite Hflag_eq.
          f_equal.
        + unfold upd_flag, State.upd_flag, bindM, returnM in Haux; simpl in Haux.
          inversion Haux; subst; simpl.
          f_equal.
      - unfold errorM.
        destruct comp_ne.
        + destruct Val.modu; [| inversion Haux].
          unfold upd_reg, bindM, returnM in Haux; simpl in Haux.
          destruct Val.longofintu as [| vi | vl | vf | vs | vp]; inversion Haux.
          subst.
          unfold State.upd_reg; simpl.
          rewrite Hflag_eq.
          f_equal.
        + unfold upd_flag, State.upd_flag, bindM, returnM in Haux; simpl in Haux.
          inversion Haux; subst; simpl.
          f_equal.
      - unfold upd_reg, bindM, returnM in Haux; simpl in Haux.
        destruct Val.longofintu as [| vi | vl | vf | vs | vp]; inversion Haux.
        subst.
        unfold State.upd_reg; simpl.
        rewrite Hflag_eq.
        f_equal.
      - unfold upd_reg, bindM, returnM in Haux; simpl in Haux.
        destruct Val.longofintu as [| vi | vl | vf | vs | vp]; inversion Haux.
        subst.
        unfold State.upd_reg; simpl.
        rewrite Hflag_eq.
        f_equal.
      - destruct compu_lt.
        + unfold upd_reg, bindM, returnM in Haux; simpl in Haux.
          destruct Val.longofint as [| vi | vl | vf | vs | vp]; inversion Haux.
          subst.
          unfold State.upd_reg; simpl.
          rewrite Hflag_eq.
          f_equal.
        + unfold upd_flag, State.upd_flag, bindM, returnM in Haux; simpl in Haux.
          inversion Haux; subst; simpl.
          f_equal.
    }
    {
      destruct b.
      - unfold upd_reg, bindM, returnM in Haux; simpl in Haux.
        destruct Val.longofintu as [| vi | vl | vf | vs | vp]; inversion Haux.
        subst.
        unfold State.upd_reg; simpl.
        rewrite Hflag_eq.
        f_equal.
      - unfold upd_reg, bindM, returnM in Haux; simpl in Haux.
        destruct Val.longofintu as [| vi | vl | vf | vs | vp]; inversion Haux.
        subst.
        unfold State.upd_reg; simpl.
        rewrite Hflag_eq.
        f_equal.
      - unfold upd_reg, bindM, returnM in Haux; simpl in Haux.
        destruct Val.longofintu as [| vi | vl | vf | vs | vp]; inversion Haux.
        subst.
        unfold State.upd_reg; simpl.
        rewrite Hflag_eq.
        f_equal.
      - unfold errorM.
        destruct comp_ne.
        + destruct Val.divu; [| inversion Haux].
          unfold upd_reg, bindM, returnM in Haux; simpl in Haux.
          destruct Val.longofintu as [| vi | vl | vf | vs | vp]; inversion Haux.
          subst.
          unfold State.upd_reg; simpl.
          rewrite Hflag_eq.
          f_equal.
        + unfold upd_flag, State.upd_flag, bindM, returnM in Haux; simpl in Haux.
          inversion Haux; subst; simpl.
          f_equal.
      - unfold upd_reg, bindM, returnM in Haux; simpl in Haux.
        destruct Val.longofintu as [| vi | vl | vf | vs | vp]; inversion Haux.
        subst.
        unfold State.upd_reg; simpl.
        rewrite Hflag_eq.
        f_equal.
      - unfold upd_reg, bindM, returnM in Haux; simpl in Haux.
        destruct Val.longofintu as [| vi | vl | vf | vs | vp]; inversion Haux.
        subst.
        unfold State.upd_reg; simpl.
        rewrite Hflag_eq.
        f_equal.
      - destruct compu_lt.
        + unfold upd_reg, bindM, returnM in Haux; simpl in Haux.
          destruct Val.longofintu as [| vi | vl | vf | vs | vp]; inversion Haux.
          subst.
          unfold State.upd_reg; simpl.
          rewrite Hflag_eq.
          f_equal.
        + unfold upd_flag, State.upd_flag, bindM, returnM in Haux; simpl in Haux.
          inversion Haux; subst; simpl.
          f_equal.
      - destruct compu_lt.
        + unfold upd_reg, bindM, returnM in Haux; simpl in Haux.
          destruct Val.longofintu as [| vi | vl | vf | vs | vp]; inversion Haux.
          subst.
          unfold State.upd_reg; simpl.
          rewrite Hflag_eq.
          f_equal.
        + unfold upd_flag, State.upd_flag, bindM, returnM in Haux; simpl in Haux.
          inversion Haux; subst; simpl.
          f_equal.
      - unfold errorM.
        destruct comp_ne.
        + destruct Val.modu; [| inversion Haux].
          unfold upd_reg, bindM, returnM in Haux; simpl in Haux.
          destruct Val.longofintu as [| vi | vl | vf | vs | vp]; inversion Haux.
          subst.
          unfold State.upd_reg; simpl.
          rewrite Hflag_eq.
          f_equal.
        + unfold upd_flag, State.upd_flag, bindM, returnM in Haux; simpl in Haux.
          inversion Haux; subst; simpl.
          f_equal.
      - unfold upd_reg, bindM, returnM in Haux; simpl in Haux.
        destruct Val.longofintu as [| vi | vl | vf | vs | vp]; inversion Haux.
        subst.
        unfold State.upd_reg; simpl.
        rewrite Hflag_eq.
        f_equal.
      - unfold upd_reg, State.upd_reg, bindM, returnM in Haux; simpl in Haux.
        inversion Haux; subst; simpl.
        rewrite Hflag_eq.
        f_equal.
      - destruct compu_lt.
        + unfold upd_reg, bindM, returnM in Haux; simpl in Haux.
          destruct Val.longofint as [| vi | vl | vf | vs | vp]; inversion Haux.
          subst.
          unfold State.upd_reg; simpl.
          rewrite Hflag_eq.
          f_equal.
        + unfold upd_flag, State.upd_flag, bindM, returnM in Haux; simpl in Haux.
          inversion Haux; subst; simpl.
          f_equal.
    }
  + unfold Semantics.eval_src, eval_reg, State.eval_reg, upd_reg, State.upd_reg,
      upd_flag, State.upd_flag, bindM, returnM in Haux; simpl in Haux.
    unfold eval_src.
    destruct s.
    {
      destruct b.
      - destruct Val.addl as [| vi | vl | vf | vs | vp]; inversion Haux.
        subst.
        rewrite Hflag_eq.
        f_equal.
      - destruct Val.subl as [| vi | vl | vf | vs | vp]; inversion Haux.
        subst.
        rewrite Hflag_eq.
        f_equal.
      - destruct Val.mull as [| vi | vl | vf | vs | vp]; inversion Haux.
        subst.
        rewrite Hflag_eq.
        f_equal.
      - unfold errorM.
        destruct compl_ne.
        + destruct Val.divlu as [ lv | ]; [| inversion Haux].
          destruct lv as [| vi | vl | vf | vs | vp]; inversion Haux.
          subst.
          simpl.
          rewrite Hflag_eq.
          f_equal.
        + inversion Haux; subst; simpl.
          f_equal.
      - destruct Val.orl as [| vi | vl | vf | vs | vp]; inversion Haux.
        subst.
        rewrite Hflag_eq.
        f_equal.
      - destruct Val.andl as [| vi | vl | vf | vs | vp]; inversion Haux.
        subst.
        rewrite Hflag_eq.
        f_equal.
      - destruct complu_lt.
        + destruct Val.shll as [| vi | vl | vf | vs | vp]; inversion Haux.
          subst.
          rewrite Hflag_eq.
          f_equal.
        + inversion Haux; subst; simpl.
          f_equal.
      - destruct complu_lt.
        + destruct Val.shrlu as [| vi | vl | vf | vs | vp]; inversion Haux.
          subst.
          rewrite Hflag_eq.
          f_equal.
        + inversion Haux; subst; simpl.
          f_equal.
      - unfold errorM.
        destruct compl_ne.
        + destruct Val.modlu as [lv | ]; [| inversion Haux].
          destruct lv as [| vi | vl | vf | vs | vp]; inversion Haux.
          subst.
          rewrite Hflag_eq.
          f_equal.
        + inversion Haux; subst; simpl.
          f_equal.
      - destruct Val.xorl as [| vi | vl | vf | vs | vp]; inversion Haux.
        subst.
        rewrite Hflag_eq.
        f_equal.
      - destruct eval_regmap as [| vi | vl | vf | vs | vp]; inversion Haux.
        subst.
        simpl.
        rewrite Hflag_eq.
        f_equal.
      - destruct complu_lt.
        + destruct Val.shrl as [| vi | vl | vf | vs | vp]; inversion Haux.
          subst.
          rewrite Hflag_eq.
          f_equal.
        + inversion Haux; subst; simpl.
          f_equal.
    }
    {
      destruct b.
      - destruct Val.addl as [| vi | vl | vf | vs | vp]; inversion Haux.
        subst.
        rewrite Hflag_eq.
        f_equal.
      - destruct Val.subl as [| vi | vl | vf | vs | vp]; inversion Haux.
        subst.
        rewrite Hflag_eq.
        f_equal.
      - destruct Val.mull as [| vi | vl | vf | vs | vp]; inversion Haux.
        subst.
        rewrite Hflag_eq.
        f_equal.
      - unfold errorM.
        destruct compl_ne.
        + destruct Val.divlu as [ lv | ]; [| inversion Haux].
          destruct lv as [| vi | vl | vf | vs | vp]; inversion Haux.
          subst.
          simpl.
          rewrite Hflag_eq.
          f_equal.
        + inversion Haux; subst; simpl.
          f_equal.
      - destruct Val.orl as [| vi | vl | vf | vs | vp]; inversion Haux.
        subst.
        rewrite Hflag_eq.
        f_equal.
      - destruct Val.andl as [| vi | vl | vf | vs | vp]; inversion Haux.
        subst.
        rewrite Hflag_eq.
        f_equal.
      - destruct complu_lt.
        + destruct Val.shll as [| vi | vl | vf | vs | vp]; inversion Haux.
          subst.
          rewrite Hflag_eq.
          f_equal.
        + inversion Haux; subst; simpl.
          f_equal.
      - destruct complu_lt.
        + destruct Val.shrlu as [| vi | vl | vf | vs | vp]; inversion Haux.
          subst.
          rewrite Hflag_eq.
          f_equal.
        + inversion Haux; subst; simpl.
          f_equal.
      - unfold errorM.
        destruct compl_ne.
        + destruct Val.modlu as [lv | ]; [| inversion Haux].
          destruct lv as [| vi | vl | vf | vs | vp]; inversion Haux.
          subst.
          rewrite Hflag_eq.
          f_equal.
        + inversion Haux; subst; simpl.
          f_equal.
      - destruct Val.xorl as [| vi | vl | vf | vs | vp]; inversion Haux.
        subst.
        rewrite Hflag_eq.
        f_equal.
      - inversion Haux; subst; simpl.
        rewrite Hflag_eq.
        f_equal.
      - destruct complu_lt.
        + destruct Val.shrl as [| vi | vl | vf | vs | vp]; inversion Haux.
          subst.
          rewrite Hflag_eq.
          f_equal.
        + inversion Haux; subst; simpl.
          f_equal.
    }
Qed.

Definition match_states (s1 s2 : state) : Prop := (**r match_states is the standard name of CompCert's relation *)
  pc_loc  s1 = pc_loc  s2 /\ 
  flag    s1 = flag    s2 /\
  regs_st s1 = regs_st s2 /\
  mrs_num s1 = mrs_num s2 /\
  bpf_mrs s1 = bpf_mrs s2 /\
  ins_len s1 = ins_len s2 /\
  ins     s1 = ins     s2 /\
  bpf_m   s1 = bpf_m   s2 .

Lemma check_all_memory_regions_guarantees_simulation_relation:
  forall n cache_id opt_flag p addr chunk st1 st2 ptr
  (Hcheck : Semantics.check_all_memory_regions n cache_id p chunk addr opt_flag st1 = Some (ptr, st2)),
    match_states st1 st2.
Proof.
  unfold match_states; induction n; simpl; unfold bindM, returnM; intros.
  { inversion Hcheck; subst; intuition. }

  unfold rBPFMonadOp.get_mem_region, upd_cache, State.upd_cache, bindM, returnM in Hcheck.
  destruct ((_ <? _)%nat); [| inversion Hcheck].
  destruct nth_error; [| inversion Hcheck].
  destruct opt_flag.
  - match goal with
    | H: (if ?X then _ else _) _ = _ |- _ =>
      destruct X
    end.
    + eapply IHn; eauto.
    + rewrite check_one_memory_region_eq in Hcheck.
      unfold cmp_ptr32_nullM in Hcheck.
      destruct cmp_ptr32_null; [| inversion Hcheck].
      destruct b.
      * eapply IHn; eauto.
      * destruct ListNat.assign; [| inversion Hcheck].
        inversion Hcheck; subst; simpl; intuition.
  - rewrite check_one_memory_region_eq in Hcheck.
    unfold cmp_ptr32_nullM in Hcheck.
    destruct cmp_ptr32_null; [| inversion Hcheck].
    destruct b.
    + eapply IHn; eauto.
    + inversion Hcheck; subst; simpl; intuition.
Qed.

Lemma check_mem_guarantees_simulation_relation:
  forall opt_flag p addr chunk st1 st2 ptr
  (Hcheck: Semantics.check_mem p chunk addr opt_flag st1 = Some (ptr, st2)),
    match_states st1 st2.
Proof.
  unfold Semantics.check_mem, eval_cache, eval_mrs_num, cmp_ptr32_nullM,
    State.eval_cache, rBPFMonadOp.get_mem_region, State.eval_mem, bindM, returnM; intros.
  destruct opt_flag.
  2:{ eapply check_all_memory_regions_guarantees_simulation_relation; eauto. }

  destruct ListNat.index; [| inversion Hcheck].
  unfold eval_mem_num in Hcheck.
  destruct ((_ =? _)%nat).
  - eapply check_all_memory_regions_guarantees_simulation_relation; eauto.
  - destruct ((_ <? _)%nat); [| inversion Hcheck].
    destruct nth_error; [| inversion Hcheck].
    rewrite check_one_memory_region_eq in Hcheck.
    destruct cmp_ptr32_null; [| inversion Hcheck].
    destruct b.
    + eapply check_all_memory_regions_guarantees_simulation_relation; eauto.
    + unfold match_states; inversion Hcheck; subst; simpl; intuition.
Qed.

Axiom lemma_bpf_get_call_simpl :
  forall i st1,
    rBPFMonadOp._bpf_get_call (Vint i) st1 = Some (_bpf_get_call (Vint i), st1).

Axiom lemma_exec_function_simpl :
  forall b ofs st1,
    exists st2, rBPFMonadOp.exec_function (Vptr b ofs) st1 = Some (exec_function (Vptr b ofs), st2) /\
      pc_loc st1 = pc_loc st2 /\ regs_st st1 = regs_st st2 /\
      bpf_m st1 = bpf_m st2 /\ flag st1 = flag st2 /\ cache st1 = cache st2 /\
      ins st1 = ins st2 /\ ins_len st1 = ins_len st2 /\
      mrs_num st1 = mrs_num st2 /\ bpf_mrs st1 = bpf_mrs st2.

Lemma check_all_memory_regions_simpl_unchanged:
  forall n p addr chunk st1 st2 ptr cache_id opt_flag
  (Hcheck: Semantics.check_all_memory_regions n cache_id p chunk addr opt_flag st1 =
         Some (ptr, st2)),
    ins st1 = ins st2 /\ ins_len st1 = ins_len st2 /\
      mrs_num st1 = mrs_num st2 /\ bpf_mrs st1 = bpf_mrs st2.
Proof.
  induction n; simpl; unfold bindM, returnM; intros.
  { inversion Hcheck; subst; intuition. }

  unfold rBPFMonadOp.get_mem_region in Hcheck.
  unfold get_mem_region.
  destruct ((_ <? _)%nat); [| inversion Hcheck].
  destruct nth_error; [| inversion Hcheck].

  destruct opt_flag.
  {
    match goal with
    | H: (if ?X then _ else _) _ = _ |- _ =>
      destruct X
    end.
    - eapply IHn; eauto.
    - rewrite check_one_memory_region_eq in Hcheck.
      unfold cmp_ptr32_nullM, State.eval_mem in Hcheck.
      destruct cmp_ptr32_null as [check_ptr| ]; [| inversion Hcheck].
      destruct check_ptr.
      + eapply IHn; eauto.
      + unfold upd_cache, State.upd_cache in Hcheck.
        destruct ListNat.assign as [cache1| ]; inversion Hcheck; subst; simpl; intuition.
  }

  rewrite check_one_memory_region_eq in Hcheck.
  unfold cmp_ptr32_nullM, State.eval_mem in Hcheck.
  destruct cmp_ptr32_null as [check_ptr| ]; [| inversion Hcheck].
  destruct check_ptr.
  + eapply IHn; eauto.
  + inversion Hcheck; subst; simpl; intuition.
Qed.

Lemma check_mem_simpl_unchanged:
  forall opt_flag p addr chunk st1 st2 ptr
  (Hcheck: Semantics.check_mem p chunk addr opt_flag st1 = Some (ptr, st2)),
    ins st1 = ins st2 /\ ins_len st1 = ins_len st2 /\
      mrs_num st1 = mrs_num st2 /\ bpf_mrs st1 = bpf_mrs st2.
Proof.
  unfold Semantics.check_mem, check_mem, bindM, returnM; intros.
  destruct opt_flag.
  - unfold eval_cache, State.eval_cache, ListNat.index in Hcheck.
    destruct nth_error as [ cache_id | ]; [| inversion Hcheck].
    destruct ((0 =? _)%nat).
    + unfold eval_mrs_num in Hcheck.
      eapply check_all_memory_regions_simpl_unchanged; eauto.
    + unfold rBPFMonadOp.get_mem_region in Hcheck.
      unfold get_mem_region.
      destruct ((_ <? _)%nat); [| inversion Hcheck].
      destruct nth_error as [ cur_mr |]; [| inversion Hcheck].
      rewrite check_one_memory_region_eq in Hcheck.
      unfold cmp_ptr32_nullM, State.eval_mem in Hcheck.
      destruct cmp_ptr32_null as [check_ptr| ]; [| inversion Hcheck].
      destruct check_ptr.
      * unfold eval_mrs_num in Hcheck.
        eapply check_all_memory_regions_simpl_unchanged; eauto.
      * inversion Hcheck; subst; simpl; intuition.
  - unfold eval_mrs_num, eval_mem_num in Hcheck.
    eapply check_all_memory_regions_simpl_unchanged; eauto.
Qed.


Lemma step_alu_binary_operation_simpl_unchanged:
  forall a b r s st1 vt st2
  (Haux : Semantics.step_alu_binary_operation a b r s st1 = Some (vt, st2)),
    ins st1 = ins st2 /\ ins_len st1 = ins_len st2 /\
    mrs_num st1 = mrs_num st2 /\ bpf_mrs st1 = bpf_mrs st2.
Proof.
  intros.
  unfold Semantics.step_alu_binary_operation, bindM, returnM in Haux.
  unfold step_alu_binary_operation.
  destruct a.
  + unfold Semantics.eval_src32, Semantics.eval_reg32, bindM, returnM in Haux; simpl in Haux.
    unfold eval_src32, eval_reg32.
    destruct s.
    {
      destruct b; unfold upd_reg, State.upd_reg, bindM, returnM in Haux; simpl in Haux.
      - destruct Val.longofintu as [| vi | vl | vf | vs | vp]; inversion Haux; subst; simpl; intuition.
      - destruct Val.longofintu as [| vi | vl | vf | vs | vp]; inversion Haux; subst; simpl; intuition.
      - destruct Val.longofintu as [| vi | vl | vf | vs | vp]; inversion Haux; subst; simpl; intuition.
      - unfold errorM.
        destruct comp_ne.
        + destruct Val.divu; [| inversion Haux].
          destruct Val.longofintu as [| vi | vl | vf | vs | vp]; inversion Haux; subst; simpl; intuition.
        + unfold upd_flag, State.upd_flag, bindM, returnM in Haux; simpl in Haux.
          inversion Haux; subst; simpl; intuition.
      - destruct Val.longofintu as [| vi | vl | vf | vs | vp]; inversion Haux; subst; simpl; intuition.
      - destruct Val.longofintu as [| vi | vl | vf | vs | vp]; inversion Haux; subst; simpl; intuition.
      - destruct compu_lt.
        + destruct Val.longofintu as [| vi | vl | vf | vs | vp]; inversion Haux; subst; simpl; intuition.
        + unfold upd_flag, State.upd_flag, bindM, returnM in Haux; simpl in Haux.
          inversion Haux; subst; simpl; intuition.
      - destruct compu_lt.
        + destruct Val.longofintu as [| vi | vl | vf | vs | vp]; inversion Haux; subst; simpl; intuition.
        + unfold upd_flag, State.upd_flag, bindM, returnM in Haux; simpl in Haux.
          inversion Haux; subst; simpl; intuition.
      - unfold errorM.
        destruct comp_ne.
        + destruct Val.modu; [| inversion Haux].
          destruct Val.longofintu as [| vi | vl | vf | vs | vp]; inversion Haux; subst; simpl; intuition.
        + unfold upd_flag, State.upd_flag, bindM, returnM in Haux; simpl in Haux.
          inversion Haux; subst; simpl; intuition.
      - destruct Val.longofintu as [| vi | vl | vf | vs | vp]; inversion Haux; subst; simpl; intuition.
      - destruct Val.longofintu as [| vi | vl | vf | vs | vp]; inversion Haux; subst; simpl; intuition.
      - destruct compu_lt.
        + destruct Val.longofint as [| vi | vl | vf | vs | vp]; inversion Haux; subst; simpl; intuition.
        + unfold upd_flag, State.upd_flag, bindM, returnM in Haux; simpl in Haux.
          inversion Haux; subst; simpl; intuition.
    }
    {
      destruct b; unfold upd_reg, State.upd_reg, bindM, returnM in Haux; simpl in Haux.
      - destruct Val.longofintu as [| vi | vl | vf | vs | vp]; inversion Haux; subst; simpl; intuition.
      - destruct Val.longofintu as [| vi | vl | vf | vs | vp]; inversion Haux; subst; simpl; intuition.
      - destruct Val.longofintu as [| vi | vl | vf | vs | vp]; inversion Haux; subst; simpl; intuition.
      - unfold errorM.
        destruct negb.
        + destruct Val.divu; [| inversion Haux].
          destruct Val.longofintu as [| vi | vl | vf | vs | vp]; inversion Haux; subst; simpl; intuition.
        + unfold upd_flag, State.upd_flag, bindM, returnM in Haux; simpl in Haux.
          inversion Haux; subst; simpl; intuition.
      - destruct Val.longofintu as [| vi | vl | vf | vs | vp]; inversion Haux; subst; simpl; intuition.
      - destruct Val.longofintu as [| vi | vl | vf | vs | vp]; inversion Haux; subst; simpl; intuition.
      - destruct Int.ltu.
        + destruct Val.longofintu as [| vi | vl | vf | vs | vp]; inversion Haux; subst; simpl; intuition.
        + unfold upd_flag, State.upd_flag, bindM, returnM in Haux; simpl in Haux.
          inversion Haux; subst; simpl; intuition.
      - destruct Int.ltu.
        + destruct Val.longofintu as [| vi | vl | vf | vs | vp]; inversion Haux; subst; simpl; intuition.
        + unfold upd_flag, State.upd_flag, bindM, returnM in Haux; simpl in Haux.
          inversion Haux; subst; simpl; intuition.
      - unfold errorM.
        destruct negb.
        + destruct Val.modu; [| inversion Haux].
          destruct Val.longofintu as [| vi | vl | vf | vs | vp]; inversion Haux; subst; simpl; intuition.
        + unfold upd_flag, State.upd_flag, bindM, returnM in Haux; simpl in Haux.
          inversion Haux; subst; simpl; intuition.
      - destruct Val.longofintu as [| vi | vl | vf | vs | vp]; inversion Haux; subst; simpl; intuition.
      - inversion Haux; subst; simpl; intuition.
      - destruct Int.ltu.
        + destruct Val.longofint as [| vi | vl | vf | vs | vp]; inversion Haux; subst; simpl; intuition.
        + unfold upd_flag, State.upd_flag, bindM, returnM in Haux; simpl in Haux.
          inversion Haux; subst; simpl; intuition.
    }
  + unfold Semantics.eval_src, eval_reg, State.eval_reg, upd_reg, State.upd_reg,
      upd_flag, State.upd_flag, bindM, returnM in Haux; simpl in Haux.
    unfold eval_src.
    destruct s.
    {
      destruct b.
      - destruct Val.addl as [| vi | vl | vf | vs | vp]; inversion Haux; subst; simpl; intuition.
      - destruct Val.subl as [| vi | vl | vf | vs | vp]; inversion Haux; subst; simpl; intuition.
      - destruct Val.mull as [| vi | vl | vf | vs | vp]; inversion Haux; subst; simpl; intuition.
      - unfold errorM.
        destruct compl_ne.
        + destruct Val.divlu as [ lv | ]; [| inversion Haux].
          destruct lv as [| vi | vl | vf | vs | vp]; inversion Haux; subst; simpl; intuition.
        + inversion Haux; subst; simpl; intuition.
      - destruct Val.orl as [| vi | vl | vf | vs | vp]; inversion Haux; subst; simpl; intuition.
      - destruct Val.andl as [| vi | vl | vf | vs | vp]; inversion Haux; subst; simpl; intuition.
      - destruct complu_lt.
        + destruct Val.shll as [| vi | vl | vf | vs | vp]; inversion Haux; subst; simpl; intuition.
        + inversion Haux; subst; simpl; intuition.
      - destruct complu_lt.
        + destruct Val.shrlu as [| vi | vl | vf | vs | vp]; inversion Haux; subst; simpl; intuition.
        + inversion Haux; subst; simpl; intuition.
      - unfold errorM.
        destruct compl_ne.
        + destruct Val.modlu as [lv | ]; [| inversion Haux].
          destruct lv as [| vi | vl | vf | vs | vp]; inversion Haux; subst; simpl; intuition.
        + inversion Haux; subst; simpl; intuition.
      - destruct Val.xorl as [| vi | vl | vf | vs | vp]; inversion Haux; subst; simpl; intuition.
      - destruct eval_regmap as [| vi | vl | vf | vs | vp]; inversion Haux; subst; simpl; intuition.
      - destruct complu_lt.
        + destruct Val.shrl as [| vi | vl | vf | vs | vp]; inversion Haux; subst; simpl; intuition.
        + inversion Haux; subst; simpl; intuition.
    }
    {
      destruct b.
      - destruct Val.addl as [| vi | vl | vf | vs | vp]; inversion Haux; subst; simpl; intuition.
      - destruct Val.subl as [| vi | vl | vf | vs | vp]; inversion Haux; subst; simpl; intuition.
      - destruct Val.mull as [| vi | vl | vf | vs | vp]; inversion Haux; subst; simpl; intuition.
      - unfold errorM.
        destruct compl_ne.
        + destruct Val.divlu as [ lv | ]; [| inversion Haux].
          destruct lv as [| vi | vl | vf | vs | vp]; inversion Haux; subst; simpl; intuition.
        + inversion Haux; subst; simpl; intuition.
      - destruct Val.orl as [| vi | vl | vf | vs | vp]; inversion Haux; subst; simpl; intuition.
      - destruct Val.andl as [| vi | vl | vf | vs | vp]; inversion Haux; subst; simpl; intuition.
      - destruct complu_lt.
        + destruct Val.shll as [| vi | vl | vf | vs | vp]; inversion Haux; subst; simpl; intuition.
        + inversion Haux; subst; simpl; intuition.
      - destruct complu_lt.
        + destruct Val.shrlu as [| vi | vl | vf | vs | vp]; inversion Haux; subst; simpl; intuition.
        + inversion Haux; subst; simpl; intuition.
      - unfold errorM.
        destruct compl_ne.
        + destruct Val.modlu as [lv | ]; [| inversion Haux].
          destruct lv as [| vi | vl | vf | vs | vp]; inversion Haux; subst; simpl; intuition.
        + inversion Haux; subst; simpl; intuition.
      - destruct Val.xorl as [| vi | vl | vf | vs | vp]; inversion Haux; subst; simpl; intuition.
      - inversion Haux; subst; simpl; intuition.
      - destruct complu_lt.
        + destruct Val.shrl as [| vi | vl | vf | vs | vp]; inversion Haux; subst; simpl; intuition.
        + inversion Haux; subst; simpl; intuition.
    }
Qed.

Lemma step_simpl_unchanged:
  forall opt_flag st1 st2 vt
  (Haux: Semantics.step opt_flag st1 = Some (vt, st2)),
    ins st1 = ins st2 /\ ins_len st1 = ins_len st2 /\
      mrs_num st1 = mrs_num st2 /\ bpf_mrs st1 = bpf_mrs st2.
Proof.
  unfold Semantics.step, step, bindM, returnM; intros.
  unfold rBPFMonadOp.eval_ins, State.eval_ins, List64AsArray.index in Haux.
  unfold eval_ins.
  destruct Int.cmpu; [| inversion Haux].
  destruct nth_error; [| inversion Haux].
  unfold decodeM in Haux.
  destruct decode as [ins | ]; [| inversion Haux].
  destruct ins.
  - destruct a; unfold eval_reg, upd_reg, State.upd_reg in Haux.
    + destruct Val.longofintu as [| vi | vl | vf | vs | vp]; inversion Haux.
      subst; simpl.
      intuition.
    + destruct Val.negl as [| vi | vl | vf | vs | vp]; inversion Haux.
      subst; simpl.
      intuition.
  - eapply step_alu_binary_operation_simpl_unchanged; eauto.
  - unfold upd_pc, bindM, returnM in Haux.
    destruct Int.cmpu; inversion Haux; subst; simpl; intuition.
  - unfold Semantics.step_branch_cond, Semantics.eval_src,
      eval_reg, State.eval_reg, bindM, returnM in Haux.
    unfold step_branch_cond, eval_src.
    destruct s;
      match goal with
      | H: (if ?X then _ else _) _ = _ |- _ =>
        destruct X
      end.
    + unfold upd_pc, bindM, returnM in Haux.
      destruct Int.cmpu; inversion Haux; subst; simpl; intuition.
    + inversion Haux; subst; simpl; intuition.
    + unfold upd_pc, bindM, returnM in Haux.
      destruct Int.cmpu; inversion Haux; subst; simpl; intuition.
    + inversion Haux; subst; simpl; intuition.
  - unfold upd_reg, State.upd_reg, bindM, returnM in Haux.
    destruct Val.longofintu as [| vi | vl | vf | vs | vp]; inversion Haux; subst; simpl; intuition.
  - unfold upd_reg, State.upd_reg, eval_reg, State.eval_reg, bindM, returnM in Haux.
    destruct Val.orl as [| vi | vl | vf | vs | vp]; inversion Haux; subst; simpl; intuition.
  - unfold Semantics.step_load_x_operation, eval_mem, eval_reg, upd_flag, State.upd_flag,
      get_addr_ofs, State.eval_reg, cmp_ptr32_nullM, State.eval_mem, rBPFMonadOp.load_mem,
      State.load_mem, bindM, returnM in Haux.
    unfold step_load_x_operation, load_mem.
    destruct Semantics.check_mem as [ (check_ptr & stk) |] eqn: Hcheck; [| inversion Haux].
    eapply check_mem_simpl_unchanged in Hcheck as Hcheck_simpl; eauto.
    destruct Hcheck_simpl as (Heq_ins & Heq_len & Heq_num & Heq_mrs).
    destruct cmp_ptr32_null as [ cond |]; [ destruct cond | inversion Haux].
    + inversion Haux; subst; simpl.
      rewrite Heq_ins, Heq_len, Heq_num, Heq_mrs.
      intuition.
    + unfold upd_reg, State.upd_reg in Haux.
      destruct m; inversion Haux; clear H0.
      {
        destruct Mem.loadv as [lv| ]; [| inversion Haux].
        unfold _to_vlong in *.
        destruct lv as [| vi | vl | vf | vs | vp]; inversion Haux; simpl;
          rewrite Heq_ins, Heq_len, Heq_num, Heq_mrs;
          intuition.
      }
      {
        destruct Mem.loadv as [lv| ]; [| inversion Haux].
        unfold _to_vlong in *.
        destruct lv as [| vi | vl | vf | vs | vp]; inversion Haux; simpl;
          rewrite Heq_ins, Heq_len, Heq_num, Heq_mrs;
          intuition.
      }
      {
        destruct Mem.loadv as [lv| ]; [| inversion Haux].
        unfold _to_vlong in *.
        destruct lv as [| vi | vl | vf | vs | vp]; inversion Haux; simpl;
          rewrite Heq_ins, Heq_len, Heq_num, Heq_mrs;
          intuition.
      }
      {
        destruct Mem.loadv as [lv| ]; [| inversion Haux].
        unfold _to_vlong in *.
        destruct lv as [| vi | vl | vf | vs | vp]; inversion Haux; simpl;
          rewrite Heq_ins, Heq_len, Heq_num, Heq_mrs;
          intuition.
      }

  - unfold Semantics.step_store_operation, eval_mem, eval_reg, upd_flag, State.upd_flag,
      get_addr_ofs, State.eval_reg, cmp_ptr32_nullM, State.eval_mem, rBPFMonadOp.store_mem_reg,
      rBPFMonadOp.store_mem_imm, State.store_mem_reg, State.store_mem_imm, bindM, returnM in Haux.
    unfold step_store_operation, store_mem_reg, store_mem_imm.

    destruct s.
    {
      destruct Semantics.check_mem as [ (check_ptr & stk) |] eqn: Hcheck; [| inversion Haux].
      eapply check_mem_simpl_unchanged in Hcheck as Hcheck_simpl; eauto.
      destruct Hcheck_simpl as (Heq_ins & Heq_len & Heq_num & Heq_mrs).
      destruct cmp_ptr32_null as [ cond |]; [ destruct cond | inversion Haux].
      + inversion Haux; subst; simpl.
        rewrite Heq_ins, Heq_len, Heq_num, Heq_mrs.
        intuition.
      + unfold upd_reg, State.upd_reg in Haux.
        destruct m; inversion Haux; clear H0.
        {
          destruct Mem.storev as [lv| ]; [| inversion Haux].
          unfold upd_mem in *.
          inversion Haux; simpl;
          rewrite Heq_ins, Heq_len, Heq_num, Heq_mrs;
          intuition.
        }
        {
          destruct Mem.storev as [lv| ]; [| inversion Haux].
          unfold upd_mem in *.
          inversion Haux; simpl;
          rewrite Heq_ins, Heq_len, Heq_num, Heq_mrs;
          intuition.
        }
        {
          destruct Mem.storev as [lv| ]; [| inversion Haux].
          unfold upd_mem in *.
          inversion Haux; simpl;
          rewrite Heq_ins, Heq_len, Heq_num, Heq_mrs;
          intuition.
        }
        {
          destruct Mem.storev as [lv| ]; [| inversion Haux].
          unfold upd_mem in *.
          inversion Haux; simpl;
          rewrite Heq_ins, Heq_len, Heq_num, Heq_mrs;
          intuition.
        }
    }

    destruct Semantics.check_mem as [ (check_ptr & stk) |] eqn: Hcheck; [| inversion Haux].
    eapply check_mem_simpl_unchanged in Hcheck as Hcheck_simpl; eauto.
    destruct Hcheck_simpl as (Heq_ins & Heq_len & Heq_num & Heq_mrs).
    destruct cmp_ptr32_null as [ cond |]; [ destruct cond | inversion Haux].
    + inversion Haux; subst; simpl.
      rewrite Heq_ins, Heq_len, Heq_num, Heq_mrs;
      intuition.
    + unfold upd_mem in Haux.
      destruct m; inversion Haux; clear H0.
      {
        destruct Mem.storev as [lv| ]; [| inversion Haux].
        inversion Haux; simpl;
        rewrite Heq_ins, Heq_len, Heq_num, Heq_mrs;
        intuition.
      }
      {
        destruct Mem.storev as [lv| ]; [| inversion Haux].
        inversion Haux; simpl;
        rewrite Heq_ins, Heq_len, Heq_num, Heq_mrs;
        intuition.
      }
      {
        destruct Mem.storev as [lv| ]; [| inversion Haux].
        inversion Haux; simpl;
        rewrite Heq_ins, Heq_len, Heq_num, Heq_mrs;
        intuition.
      }
      {
        destruct Mem.storev as [lv| ]; [| inversion Haux].
        inversion Haux; simpl;
        rewrite Heq_ins, Heq_len, Heq_num, Heq_mrs;
        intuition.
      }

  - pose proof (lemma_bpf_get_call (Int.repr (Int64.unsigned (Int64.repr (Int.signed i0)))) st1) as Hcall.
    destruct Hcall as (ptr & Hcall & Hptr_prop).
    rewrite Hcall in Haux.
    unfold cmp_ptr32_nullM in Haux.
    rewrite lemma_bpf_get_call_simpl in Hcall.
    injection Hcall as Heq.
    unfold State.eval_mem in Haux.
    destruct cmp_ptr32_null as [call_ptr |] eqn: Hcmp; [destruct call_ptr | inversion Haux].
    + unfold upd_flag, State.upd_flag in Haux.
      inversion Haux; subst; simpl.
      intuition.
    + destruct Hptr_prop as [HF | Hptr].
      * exfalso.
        Transparent Archi.ptr64.
        rewrite HF in Hcmp; unfold cmp_ptr32_null, Vnullptr in Hcmp; simpl in Hcmp.
        rewrite Int.eq_true in Hcmp.
        inversion Hcmp.
      * destruct Hptr as (b & ofs & Hptr & Hvalid_pointer).
        subst ptr.
        pose proof (lemma_exec_function0 b ofs st1) as Hexec.
        destruct Hexec as (v & stk & Hexec & Hcmp_exec).
        rewrite Hptr in *.
        rewrite Hexec in Haux.
        pose proof (lemma_exec_function_simpl b ofs st1) as Heq.
        destruct Heq as (stn & Heq & Heq_pc & Heq_rs & Heq_m & Heq_flag & Heq_cache &
          Heq_ins & Heq_len & Heq_num & Heq_mrs).
        rewrite Heq in Hexec.
        injection Hexec as Heq_exe Hst_eq.
        unfold upd_reg, State.upd_reg in Haux.
        destruct Val.longofintu as [| vi | vl | vf | vs | vp]; inversion Haux.
        subst; simpl.
        rewrite Heq_ins, Heq_len, Heq_num, Heq_mrs;
        intuition.
  - unfold upd_flag, State.upd_flag in Haux.
    inversion Haux; subst; simpl; intuition.
  - unfold upd_flag, State.upd_flag in Haux.
    inversion Haux; subst; simpl; intuition.
Qed.


Lemma step_simpl_equiv:
  forall opt_flag st1 st2 vt
  (Hflag_eq: flag st1 = BPF_OK)
  (Haux: Semantics.step opt_flag st1 = Some (vt, st2)),
    step (pc_loc st1) (cache st1) (ins st1) (ins_len st1)
      (regs_st st1) (mrs_num st1) (bpf_mrs st1) 
      (bpf_m st1) opt_flag = Some ((pc_loc st2), (regs_st st2), (bpf_m st2), (flag st2), (cache st2)).
Proof.
  unfold Semantics.step, step, bindM, returnM; intros.
  unfold rBPFMonadOp.eval_ins, State.eval_ins, List64AsArray.index in Haux.
  unfold eval_ins.
  destruct Int.cmpu; [| inversion Haux].
  destruct nth_error; [| inversion Haux].
  unfold decodeM in Haux.
  destruct decode as [ins | ]; [| inversion Haux].
  destruct ins.
  - destruct a.
    + unfold eval_reg in Haux.
      unfold upd_reg in Haux.
      destruct Val.longofintu as [| vi | vl | vf | vs | vp]; inversion Haux.
      subst.
      unfold State.upd_reg; simpl.
      rewrite Hflag_eq.
      f_equal.
    + unfold eval_reg in Haux.
      unfold upd_reg in Haux.
      destruct Val.negl as [| vi | vl | vf | vs | vp]; inversion Haux.
      subst.
      unfold State.upd_reg; simpl.
      rewrite Hflag_eq.
      f_equal.
  - eapply step_alu_binary_operation_simpl_equiv; eauto.
  - unfold upd_pc, bindM, returnM in Haux.
    destruct Int.cmpu; [| inversion Haux].
    inversion Haux; subst; simpl.
    rewrite Hflag_eq.
    f_equal.
  - unfold Semantics.step_branch_cond, Semantics.eval_src,
      eval_reg, State.eval_reg, bindM, returnM in Haux.
    unfold step_branch_cond, eval_src.
    destruct s;
      match goal with
      | H: (if ?X then _ else _) _ = _ |- _ =>
        destruct X
      end.
    + unfold upd_pc, bindM, returnM in Haux.
      destruct Int.cmpu; [| inversion Haux].
      inversion Haux; subst; simpl.
      rewrite Hflag_eq.
      f_equal.
    + inversion Haux; subst; simpl.
      rewrite Hflag_eq.
      f_equal.
    + unfold upd_pc, bindM, returnM in Haux.
      destruct Int.cmpu; [| inversion Haux].
      inversion Haux; subst; simpl.
      rewrite Hflag_eq.
      f_equal.
    + inversion Haux; subst; simpl.
      rewrite Hflag_eq.
      f_equal.
  - unfold upd_reg, State.upd_reg, bindM, returnM in Haux.
    destruct Val.longofintu as [| vi | vl | vf | vs | vp]; inversion Haux.
    subst; simpl.
    rewrite Hflag_eq.
    f_equal.
  - unfold upd_reg, State.upd_reg, eval_reg, State.eval_reg, bindM, returnM in Haux.
    destruct Val.orl as [| vi | vl | vf | vs | vp]; inversion Haux.
    subst; simpl.
    rewrite Hflag_eq.
    f_equal.
  - unfold Semantics.step_load_x_operation, eval_mem, eval_reg, upd_flag, State.upd_flag,
      get_addr_ofs, State.eval_reg, cmp_ptr32_nullM, State.eval_mem, rBPFMonadOp.load_mem,
      State.load_mem, bindM, returnM in Haux.
    unfold step_load_x_operation, load_mem.
    destruct Semantics.check_mem as [ (check_ptr & stk) |] eqn: Hcheck; [| inversion Haux].
    eapply check_mem_simpl_equiv in Hcheck as Hcheck_simpl; eauto.
    rewrite Hcheck_simpl; clear Hcheck_simpl.
    eapply check_mem_guarantees_simulation_relation in Hcheck; eauto.
    unfold match_states in Hcheck.
    destruct Hcheck as (Hpc_eq & Hflag_eq1 & Hreg_eq & Hnum_eq & Hmrs_eq & Hlen_eq & Hins_eq & Hm_eq).
    rewrite <- Hm_eq in Haux.
    (**r here we need to prove check_mem only modify the cache field *)
    destruct cmp_ptr32_null as [ cond |]; [ destruct cond | inversion Haux].
    + inversion Haux; subst; simpl.
      rewrite Hpc_eq, Hreg_eq, Hm_eq.
      f_equal.
    + unfold upd_reg, State.upd_reg in Haux.
      destruct m; inversion Haux; clear H0; rewrite <- Hm_eq in Haux.
      {
        destruct Mem.loadv as [lv| ]; [| inversion Haux].
        unfold _to_vlong in *.
        destruct lv as [| vi | vl | vf | vs | vp]; inversion Haux; simpl.
        - rewrite <- Hflag_eq1.
          rewrite Hflag_eq, Hpc_eq, Hreg_eq, Hm_eq.
          f_equal.
        - rewrite <- Hflag_eq1.
          rewrite Hflag_eq, Hpc_eq, Hreg_eq, Hm_eq.
          f_equal.
      }
      {
        destruct Mem.loadv as [lv| ]; [| inversion Haux].
        unfold _to_vlong in *.
        destruct lv as [| vi | vl | vf | vs | vp]; inversion Haux; simpl.
        - rewrite <- Hflag_eq1.
          rewrite Hflag_eq, Hpc_eq, Hreg_eq, Hm_eq.
          f_equal.
        - rewrite <- Hflag_eq1.
          rewrite Hflag_eq, Hpc_eq, Hreg_eq, Hm_eq.
          f_equal.
      }
      {
        destruct Mem.loadv as [lv| ]; [| inversion Haux].
        unfold _to_vlong in *.
        destruct lv as [| vi | vl | vf | vs | vp]; inversion Haux; simpl.
        - rewrite <- Hflag_eq1.
          rewrite Hflag_eq, Hpc_eq, Hreg_eq, Hm_eq.
          f_equal.
        - rewrite <- Hflag_eq1.
          rewrite Hflag_eq, Hpc_eq, Hreg_eq, Hm_eq.
          f_equal.
      }
      {
        destruct Mem.loadv as [lv| ]; [| inversion Haux].
        destruct lv as [| vi | vl | vf | vs | vp]; inversion Haux; simpl.
        rewrite <- Hflag_eq1.
        rewrite Hflag_eq, Hpc_eq, Hreg_eq, Hm_eq.
        f_equal.
      }

  - unfold Semantics.step_store_operation, eval_mem, eval_reg, upd_flag, State.upd_flag,
      get_addr_ofs, State.eval_reg, cmp_ptr32_nullM, State.eval_mem, rBPFMonadOp.store_mem_reg,
      rBPFMonadOp.store_mem_imm, State.store_mem_reg, State.store_mem_imm, bindM, returnM in Haux.
    unfold step_store_operation, store_mem_reg, store_mem_imm.

    destruct s.
    {
      destruct Semantics.check_mem as [ (check_ptr & stk) |] eqn: Hcheck; [| inversion Haux].
      eapply check_mem_simpl_equiv in Hcheck as Hcheck_simpl; eauto.
      rewrite Hcheck_simpl; clear Hcheck_simpl.
      eapply check_mem_guarantees_simulation_relation in Hcheck; eauto.
      unfold match_states in Hcheck.
      destruct Hcheck as (Hpc_eq & Hflag_eq1 & Hreg_eq & Hnum_eq & Hmrs_eq & Hlen_eq & Hins_eq & Hm_eq).
      rewrite <- Hm_eq in Haux.
      (**r here we need to prove check_mem only modify the cache field *)
      destruct cmp_ptr32_null as [ cond |]; [ destruct cond | inversion Haux].
      + inversion Haux; subst; simpl.
        rewrite Hpc_eq, Hreg_eq, Hm_eq.
        f_equal.
      + unfold upd_reg, State.upd_reg in Haux.
        destruct m; inversion Haux; clear H0; rewrite <- Hm_eq in Haux.
        {
          destruct Mem.storev as [lv| ]; [| inversion Haux].
          unfold upd_mem in *.
          inversion Haux; subst; simpl.
          rewrite <- Hflag_eq1.
          rewrite Hflag_eq, Hpc_eq, Hreg_eq.
          f_equal.
        }
        {
          destruct Mem.storev as [lv| ]; [| inversion Haux].
          unfold upd_mem in *.
          inversion Haux; subst; simpl.
          rewrite <- Hflag_eq1.
          rewrite Hflag_eq, Hpc_eq, Hreg_eq.
          f_equal.
        }
        {
          destruct Mem.storev as [lv| ]; [| inversion Haux].
          unfold upd_mem in *.
          inversion Haux; subst; simpl.
          rewrite <- Hflag_eq1.
          rewrite Hflag_eq, Hpc_eq, Hreg_eq.
          f_equal.
        }
        {
          destruct Mem.storev as [lv| ]; [| inversion Haux].
          unfold upd_mem in *.
          inversion Haux; subst; simpl.
          rewrite <- Hflag_eq1.
          rewrite Hflag_eq, Hpc_eq, Hreg_eq.
          f_equal.
        }
    }

    destruct Semantics.check_mem as [ (check_ptr & stk) |] eqn: Hcheck; [| inversion Haux].
    eapply check_mem_simpl_equiv in Hcheck as Hcheck_simpl; eauto.
    rewrite Hcheck_simpl; clear Hcheck_simpl.
    eapply check_mem_guarantees_simulation_relation in Hcheck; eauto.
    unfold match_states in Hcheck.
    destruct Hcheck as (Hpc_eq & Hflag_eq1 & Hreg_eq & Hnum_eq & Hmrs_eq & Hlen_eq & Hins_eq & Hm_eq).
    rewrite <- Hm_eq in Haux.
    (**r here we need to prove check_mem only modify the cache field *)
    destruct cmp_ptr32_null as [ cond |]; [ destruct cond | inversion Haux].
    + inversion Haux; subst; simpl.
      rewrite Hpc_eq, Hreg_eq, Hm_eq.
      f_equal.
    + unfold upd_mem in Haux.
      destruct m; inversion Haux; clear H0; rewrite <- Hm_eq in Haux.
      {
        destruct Mem.storev as [lv| ]; [| inversion Haux].
        inversion Haux; subst; simpl.
        rewrite <- Hflag_eq1.
        rewrite Hflag_eq, Hpc_eq, Hreg_eq.
        f_equal.
      }
      {
        destruct Mem.storev as [lv| ]; [| inversion Haux].
        inversion Haux; subst; simpl.
        rewrite <- Hflag_eq1.
        rewrite Hflag_eq, Hpc_eq, Hreg_eq.
        f_equal.
      }
      {
        destruct Mem.storev as [lv| ]; [| inversion Haux].
        inversion Haux; subst; simpl.
        rewrite <- Hflag_eq1.
        rewrite Hflag_eq, Hpc_eq, Hreg_eq.
        f_equal.
      }
      {
        destruct Mem.storev as [lv| ]; [| inversion Haux].
        inversion Haux; subst; simpl.
        rewrite <- Hflag_eq1.
        rewrite Hflag_eq, Hpc_eq, Hreg_eq.
        f_equal.
      }
  - pose proof (lemma_bpf_get_call (Int.repr (Int64.unsigned (Int64.repr (Int.signed i0)))) st1) as Hcall.
    destruct Hcall as (ptr & Hcall & Hptr_prop).
    rewrite Hcall in Haux.
    unfold cmp_ptr32_nullM in Haux.
    rewrite lemma_bpf_get_call_simpl in Hcall.
    injection Hcall as Heq.
    rewrite Heq.
    unfold State.eval_mem in Haux.
    destruct cmp_ptr32_null as [call_ptr |] eqn: Hcmp; [destruct call_ptr | inversion Haux].
    + unfold upd_flag, State.upd_flag in Haux.
      inversion Haux; subst; simpl.
      f_equal.
    + destruct Hptr_prop as [HF | Hptr].
      * exfalso.
        Transparent Archi.ptr64.
        rewrite HF in Hcmp; unfold cmp_ptr32_null, Vnullptr in Hcmp; simpl in Hcmp.
        rewrite Int.eq_true in Hcmp.
        inversion Hcmp.
      * destruct Hptr as (b & ofs & Hptr & Hvalid_pointer).
        subst ptr.
        pose proof (lemma_exec_function0 b ofs st1) as Hexec.
        destruct Hexec as (v & stk & Hexec & Hcmp_exec).
        rewrite Hptr in *.
        rewrite Hexec in Haux.
        pose proof (lemma_exec_function_simpl b ofs st1) as Heq.
        destruct Heq as (stn & Heq & Heq_pc & Heq_rs & Heq_m & Heq_flag & Heq_cache & _).
        rewrite Heq in Hexec.
        injection Hexec as Heq_exe Hst_eq.
        rewrite Heq_exe.
        unfold upd_reg, State.upd_reg in Haux.
        destruct Val.longofintu as [| vi | vl | vf | vs | vp]; inversion Haux.
        subst; simpl.
        rewrite Hflag_eq in Heq_flag.
        rewrite Heq_pc, Heq_rs, Heq_m, Heq_flag, Heq_cache.
        f_equal.
  - unfold upd_flag, State.upd_flag in Haux.
    inversion Haux; subst; simpl.
    f_equal.
  - unfold upd_flag, State.upd_flag in Haux.
    inversion Haux; subst; simpl.
    f_equal.
Qed.

Lemma bpf_interpreter_aux_simpl_equiv:
  forall fuel opt_flag st1 st2 vt
  (Hflag_eq: flag st1 = BPF_OK)
  (Haux: Semantics.bpf_interpreter_aux fuel opt_flag st1 =
       Some (vt, st2)),
    bpf_interpreter_aux fuel (pc_loc st1) (cache st1) (ins st1) (ins_len st1)
      (regs_st st1) (mrs_num st1) (bpf_mrs st1)
      (bpf_m st1) opt_flag = Some ((pc_loc st2), (regs_st st2), (bpf_m st2), (flag st2), (cache st2)).
Proof.
  induction fuel; simpl; unfold bindM, returnM; intros.
  {
    unfold upd_flag in Haux.
    unfold State.upd_flag, State.upd_reg in Haux;
    inversion Haux; subst; f_equal.
  }

  unfold check_pc, State.check_pc, Int.cmpu in Haux.
  unfold State.upd_reg in Haux; simpl in Haux.
  destruct Int.ltu.
  2:{
    unfold upd_flag, State.upd_flag in Haux; simpl in Haux.
    inversion Haux; subst; f_equal.
  }

  destruct Semantics.step as [(rt & stk)|] eqn: Hstep; [| inversion Haux].
  eapply step_simpl_equiv in Hstep as Hstep_simpl; eauto; simpl in Hstep_simpl.
  rewrite Hstep_simpl; clear Hstep_simpl.
  eapply step_simpl_unchanged in Hstep; eauto; simpl in Hstep.
  destruct Hstep as (Heq_ins & Heq_len & Heq_num & Heq_mrs).
  unfold State.eval_flag in Haux.
  destruct flag_eq eqn: Hflag.
  - unfold State.check_pc_incr, Int.cmpu in Haux.
    rewrite <- Heq_len in Haux.
    destruct Int.ltu.
    + unfold upd_pc in Haux.
      destruct Int.cmpu; [| inversion Haux].
      remember (State.upd_pc Int.one stk) as stn.
      assert (Heq: Int.add (pc_loc stk) Int.one = pc_loc stn). {
        subst stn.
        unfold State.upd_pc; simpl. f_equal.
      }
      rewrite Heq; clear Heq.

      assert (Heq: cache stk = cache stn). {
        subst stn.
        unfold State.upd_pc; simpl. f_equal.
      }
      rewrite Heq; clear Heq.

      assert (Heq: ins st1 = ins stn). {
        subst stn.
        unfold State.upd_pc; simpl. auto.
      }
      rewrite Heq; clear Heq.

      assert (Heq: ins_len st1 = ins_len stn). {
        subst stn.
        unfold State.upd_pc; simpl. auto.
      }
      rewrite Heq; clear Heq.

      assert (Heq: regs_st stk = regs_st stn). {
        subst stn.
        unfold State.upd_pc; simpl. auto.
      }
      rewrite Heq; clear Heq.

      assert (Heq: mrs_num st1 = mrs_num stn). {
        subst stn.
        unfold State.upd_pc; simpl. auto.
      }
      rewrite Heq; clear Heq.

      assert (Heq: bpf_mrs st1 = bpf_mrs stn). {
        subst stn.
        unfold State.upd_pc; simpl. auto.
      }
      rewrite Heq; clear Heq.

      assert (Heq: bpf_m stk = bpf_m stn). {
        subst stn.
        unfold State.upd_pc; simpl. auto.
      }
      rewrite Heq; clear Heq.
      eapply IHfuel; eauto.

      assert (Heq: flag stk = flag stn). {
        subst stn.
        unfold State.upd_pc; simpl. auto.
      }
      rewrite <- Heq; clear Heq.
      clear - Hflag.
      unfold flag_eq in Hflag.
      destruct (flag stk); inversion Hflag.
      reflexivity.
    + unfold upd_flag, State.upd_flag in Haux; simpl in Haux.
      inversion Haux; subst; f_equal.
  - inversion Haux; subst; f_equal.
Qed.

Theorem bpf_interpreter_simpl_equiv:
  forall fuel ctx_ptr opt_flag st1 res st2 v
  (Hflag_eq: flag st1 = BPF_OK)
  (Hctx: ctx_ptr = Vint v)
  (Hinterp: Semantics.bpf_interpreter fuel ctx_ptr opt_flag st1 = Some (res, st2)),
    bpf_interpreter fuel ctx_ptr (pc_loc st1) (cache st1) (ins st1)
      (ins_len st1) (regs_st st1) (mrs_num st1) (bpf_mrs st1) (bpf_m st1) opt_flag = Some (res, (pc_loc st2), (regs_st st2), (bpf_m st2), (flag st2), (cache st2)).
Proof.
  unfold Semantics.bpf_interpreter, bpf_interpreter, upd_reg, bindM, returnM.
  intros.
  subst ctx_ptr.
  simpl in *.
  destruct Semantics.bpf_interpreter_aux as [(vt & st')|] eqn: Haux; [| inversion Hinterp].
  eapply bpf_interpreter_aux_simpl_equiv in Haux; eauto.

  unfold State.upd_reg in Haux; simpl in Haux.
  simpl.
  rewrite Haux.

  unfold State.eval_flag in Hinterp.
  destruct flag_eq; inversion Hinterp; subst; f_equal.
Qed.


(*
Theorem bpf_interpreter_simpl_none_equiv:
  forall fuel ctx_ptr opt_flag st1 v
  (Hctx: ctx_ptr = Vint v)
  (Hinterp: Semantics.bpf_interpreter fuel ctx_ptr opt_flag st1 = None),
    bpf_interpreter fuel ctx_ptr (pc_loc st1) (cache st1) (ins st1)
      (ins_len st1) (regs_st st1) (mrs_num st1) (bpf_mrs st1) (bpf_m st1) opt_flag = None.
Proof.
  unfold Semantics.bpf_interpreter, bpf_interpreter, upd_reg, bindM, returnM.
  intros.
  subst ctx_ptr.
  simpl in *. ../..
  destruct Semantics.bpf_interpreter_aux as [(vt & st')|] eqn: Haux; [| inversion Hinterp].
  eapply bpf_interpreter_aux_simpl_equiv in Haux; eauto.

  unfold State.upd_reg in Haux; simpl in Haux.
  simpl.
  rewrite Haux.

  unfold State.eval_flag in Hinterp.
  destruct flag_eq.
  - inversion Hinterp; subst; f_equal.
  - inversion Hinterp; subst; f_equal.
Qed. *)

