(**************************************************************************)
(*  This file is part of CertrBPF,                                        *)
(*  a formally verified rBPF verifier + interpreter + JIT in Coq.         *)
(*                                                                        *)
(*  Copyright (C) 2022 Inria                                              *)
(*                                                                        *)
(*  This program is free software; you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation; either version 2 of the License, or     *)
(*  (at your option) any later version.                                   *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(**************************************************************************)

(**r: equivalence between bpf.model (with formal syntax + semantics) and bpf.src (for dx) *)

From Coq Require Import ZArith Lia List.
Import ListNotations.
From Coq Require Import FunctionalExtensionality.
From compcert Require Import Integers Values Memory Memdata.

From bpf.comm Require Import ListAsArray State Monad LemmaNat rBPFMonadOp.
From bpf.comm Require Import Flag rBPFValues Regs BinrBPF State Monad MemRegion rBPFAST rBPFMemType rBPFMonadOp.

From bpf.model Require Import Decode Semantics.
From bpf.model Require Import Syntax Decode.

(*
From bpf.equivalence Require Import switch decode_if. *)

Open Scope Z_scope.
Open Scope monad_scope.

(**r there are two cases that must be discuss when proving the IH of `bpf_interpreter_aux`:
 - cache missing: we do normal check_mem, the proof will be trivil
 - cache hint:
    (error idea) we need to know the history info (previous cache updation):
    exists n,
      (pc, fuel) -> cache hint /\
      (pc, fuel+n) -> cache missing but check_mem ... = Vptr b ofs
      how to reuse it? because the register value may be changed ...
    (may be correct): if cache hint, we only need to show, the no_opt always returns the same result !!! *)

Global Transparent Archi.ptr64.
Lemma bind_ret : forall b f, @bindM bool val state (@returnM bool state b) f = f b .
Proof.
  intros.
  extensionality st.
  reflexivity.
Qed.

Lemma eval_mrs_num_unchanged_state : forall st, exists res, eval_mrs_num st = Some (res, st).
Proof.
  intros.
  unfold eval_mrs_num.
  exists (eval_mem_num st).
  f_equal.
Qed.

(**r we don't need this lemma
Lemma equiv_eval_upd :  forall mem_num st, (eval_cache st = Some (0%nat, st)) ->  exists x, (upd_cache (S mem_num) st) = Some (tt , x).
Proof.
Admitted. *)

Lemma option_no_exists:
  forall {A: Type} (a: option A),
    a <> None <->
    exists e, a = Some e.
Proof.
  split.
  - destruct a; intros.
    + exists a; f_equal.
    + exfalso; apply H; reflexivity.
  - intros.
    destruct H as (e & Heq).
    subst.
    intro HF; inversion HF.
Qed.

Lemma length_nth_same_aux:
  forall {A B: Type} n (l1: list A) (l2: list B),
    List.length l1 = List.length l2 ->
    List.nth_error l1 n <> None ->
    List.nth_error l2 n <> None.
Proof.
  induction n; simpl; intros.
  - destruct l1.
    + exfalso; apply H0; reflexivity.
    + destruct l2.
      * inversion H.
      * intro HF; inversion HF.
  - destruct l1.
    + exfalso; apply H0; reflexivity.
    + destruct l2.
      * inversion H.
      * eapply IHn; eauto.
Qed.

Lemma length_nth_same_iff:
  forall {A B: Type} n (l1: list A) (l2: list B),
    List.length l1 = List.length l2 ->
    List.nth_error l1 n <> None <->
    List.nth_error l2 n <> None.
Proof.
  split; eapply length_nth_same_aux; eauto.
Qed.

Lemma eval_ins_eval_cache:
  forall st i
    (Hsame: List.length (ins st) = List.length (cache st))
    (Hins: eval_ins st = Some i),
      exists n, eval_cache st = Some n.
Proof.
  unfold eval_ins, eval_cache, State.eval_ins, State.eval_cache, List64AsArray.index, ListNat.index.
  intros.
  destruct Int.cmpu; [| inversion Hins].
  destruct nth_error eqn: Hnth in Hins; inversion Hins; subst.
  assert (Heq: nth_error (ins st) (Z.to_nat (Int.unsigned (pc_loc st))) <> None).
  - rewrite Hnth.
    intro HF; inversion HF.
  - clear Hnth Hins.
    erewrite length_nth_same_iff in Heq; eauto.
    rewrite option_no_exists in Heq.
    destruct Heq as (e & Heq).
    rewrite Heq.
    eexists; f_equal.
Qed.

Lemma length_nth_assign_same:
  forall {A: Type} n (l1: list A) l2 v,
    List.length l1 = List.length l2 ->
    List.nth_error l1 n <> None ->
    ListNat.assign l2 n v <> None.
Proof.
  unfold ListNat.assign.
  induction n; simpl; intros.
  - destruct l1.
    + exfalso; apply H0; reflexivity.
    + destruct l2.
      * inversion H.
      * simpl.
        intro HF; inversion HF.
  - destruct l1.
    + exfalso; apply H0; reflexivity.
    + destruct l2.
      * inversion H.
      * simpl.
        eapply IHn with (v := v) in H0; eauto.
        destruct ListNat.assign'.
        { intro HF; inversion HF. }
        assumption.
Qed.

Lemma eval_ins_upd_cache:
  forall st i n
    (Hsame: List.length (ins st) = List.length (cache st))
    (Hins: eval_ins st = Some i),
      exists l, upd_cache n st = Some l.
Proof.
  unfold eval_ins, upd_cache, State.eval_ins, State.upd_cache, List64AsArray.index.
  intros.
  destruct Int.cmpu; [| inversion Hins].

  destruct nth_error eqn: Hnth in Hins; inversion Hins; subst.
  assert (Heq: nth_error (ins st) (Z.to_nat (Int.unsigned (pc_loc st))) <> None).
  - rewrite Hnth.
    intro HF; inversion HF.
  - clear Hnth Hins.
    eapply length_nth_assign_same with (v := n) in Heq; eauto.
    destruct ListNat.assign.
    + eexists; f_equal.
    + exfalso; apply Heq; f_equal.
Qed.


(*
state' 
{ regs_st : regmap }

Lemma projection : forall (d: reg) (s: reg+imm), 
 alpha state' (do _ <- upd_reg d v;
               do src <- eval_reg d;  returnM src) = 
              (do _ <- alpha state' (upd_reg d v);
               do src <- alpha state' (eval_reg d); 
                  alpha state' (returnM src)).
Lemma projection : forall (d: reg) (s: reg+imm), 
 alpha state' (do dst <- eval_reg d;
               do src <- eval_src s;  returnM (compl_eq   dst src)) = 
              (do dst <- alpha state' (eval_reg d);
               do src <- alpha state' (eval_src s); alpha state' (returnM (compl_eq   dst src))).
               *)


Definition match_states (s1 s2 : state) : Prop := (**r match_states is the standard name of CompCert's relation *)
  pc_loc  s1 = pc_loc  s2 /\ 
  flag    s1 = flag    s2 /\
  regs_st s1 = regs_st s2 /\
  mrs_num s1 = mrs_num s2 /\
  bpf_mrs s1 = bpf_mrs s2 /\
  ins_len s1 = ins_len s2 /\
  ins     s1 = ins     s2 /\
  bpf_m   s1 = bpf_m   s2 .

Ltac forward_one_step Hcontext Hgoal Hgoal_lemma :=
  let res3 := fresh "res" in
  let st3 := fresh "st" in
  let Htmp := fresh "Htmp" in
  match goal with
  | H0: match_states ?st1 ?st2 |- _ =>
    unfold bindM at 1;
    unfold bindM at 1 in Hcontext;
    destruct Hgoal as [(res3, st3) | ] eqn: Htmp; [| inversion Hcontext];
    eapply Hgoal_lemma in H0; eauto;
    destruct H0 as (st3' & Heq & H0);
    rewrite <- Heq;
    try (clear Heq Htmp; clear st1 st2; rename st3 into st1; rename st3' into st2)
  end.

Open Scope monad_scope.

Definition ground_state_rs (rs : regmap) : state :=
{|
  flag := BPF_OK;
  regs_st := rs;
  mrs_num := 0;
  bpf_mrs := nil;
  ins_len := 0; 
  ins := nil;
  cache := nil;
  bpf_m := Mem.empty;
  pc_loc := Int.zero
|}.

Definition reset_state_except_rs (s : state) : state :=
{|
  flag := BPF_OK;
  regs_st := (regs_st s);
  mrs_num := 0;
  bpf_mrs := nil;
  ins_len := 0; 
  ins := nil;
  cache := nil;
  bpf_m := Mem.empty;
  pc_loc := Int.zero
|}.

Definition upd_state_rs (s : state) (rs : regmap) : state :=
{|
  flag := (flag s);
  regs_st := rs;
  mrs_num := (mrs_num s);
  bpf_mrs := (bpf_mrs s);
  ins_len := (ins_len s); 
  ins := (ins s);
  cache := (cache s);
  bpf_m := (bpf_m s);
  pc_loc := (pc_loc s) 
|}.


Theorem option_map_comp : forall (A B C : Type) (f : B -> C) (g : A -> B) x, option_map f (option_map g x) = option_map (fun z => f (g z)) x.
Proof.
intros.
destruct x; [ reflexivity | reflexivity].
Qed.

Definition proj_rs (m : M state unit) : M regmap unit := 
fun rs : regmap => option_map (fun s => (fst s, regs_st (snd s))) (m (ground_state_rs rs)).

Definition inv_rs (m : M regmap unit) : M state unit := 
fun st : state => option_map (fun s => (fst s, upd_state_rs st (snd s))) (m (regs_st st)).

Theorem id_rs : forall (m : M state unit)
(H : forall st, m (ground_state_rs (regs_st st)) = m st)
(H2 : forall st, m st = (option_map (fun x => (fst x, upd_state_rs st (regs_st (snd x)))) (m st))),
 inv_rs (proj_rs m) = m.
Proof.
intros.
unfold proj_rs.
unfold inv_rs. 
extensionality st.
rewrite option_map_comp.
cbn.
rewrite H. 
rewrite <- H2. reflexivity.
Qed.

Definition reset_cache (s : state) : state :=
{|
  flag := flag s;
  regs_st := regs_st s;
  mrs_num := mrs_num s;
  bpf_mrs := bpf_mrs s;
  ins_len := ins_len s; 
  ins := ins s;
  cache := nil;
  bpf_m := bpf_m s;
  pc_loc := pc_loc s
|}.


Definition upd_state_cache (s : state) (c : ListNat.t) : state :=
{|
  flag := flag s;
  regs_st := regs_st s;
  mrs_num := mrs_num s;
  bpf_mrs := bpf_mrs s;
  ins_len := ins_len s; 
  ins := ins s;
  cache := c;
  bpf_m := bpf_m s;
  pc_loc := pc_loc s 
|}.

Definition upd_state_flag (s : state) (f : bpf_flag) : state :=
{|
  flag := f;
  regs_st := regs_st s;
  mrs_num := mrs_num s;
  bpf_mrs := bpf_mrs s;
  ins_len := ins_len s; 
  ins := ins s;
  cache := cache s;
  bpf_m := bpf_m s;
  pc_loc := pc_loc s 
|}.
 
Definition proj_cache (m : M state unit) : M state unit := 
fun st : state => option_map (fun s => (fst s, reset_cache (snd s))) (m st).

Definition inv_cache (m : M state unit) : M state unit := 
fun st : state => option_map (fun s => (fst s, upd_state_cache (snd s) (cache st))) (m st).

Theorem id_cache : forall (f : M state unit)
(H : forall st x, f st = Some x -> option_map (fun s : unit * state => cache (snd s)) (f st) = Some (cache st)),
 f = (inv_cache (proj_cache f)).
Proof.
intros.
extensionality st.
cbn. 
unfold inv_cache. unfold proj_cache.
rewrite option_map_comp. cbn.
destruct (f st) eqn:Hf; [ | reflexivity].
apply H in Hf as H'. 
rewrite Hf in H'.
cbn. cbn in H'.  inversion H'. 
destruct p. cbn. 
clear Hf H' H1.
destruct s.
cbn.
unfold upd_state_cache. cbn.
reflexivity.
Qed.


(* 
Shared lemmas
*)

Lemma map_bind_push : forall (A B C St : Type) (f : B * St -> C * St) (g : A -> M St B) (y : M St A) (z : St), 
  option_map f ((do x <- y; g x) z) = (do x <- y; (fun st => option_map f (g x st))) z.
Proof.
intros.
unfold bindM.
destruct (y z) as [ (x', st') | ]; [reflexivity | reflexivity].
Qed.

Lemma bind_push_f : forall (A B St : Type) (f : St -> St) (g : A -> M St B) (y : M St A) (z : St)
  (H : y (f z) = (option_map (fun st => (fst st, f (snd st))) (y z)))
  , 
  (do x <- y; g x) (f z) = (do x <- y; (fun st => (g x (f st)))) z.
Proof.
intros.
unfold bindM.
rewrite H.
destruct (y z) as [ (x', st') | ]; [reflexivity | reflexivity].
Qed.

(*
Specific lemmas per instruction
*)

Definition step_alu_binary_operation_simpl (d : reg) (s : reg + imm): M state unit := 
 do d32 <- eval_reg32 d;
 do s32 <- eval_src32 s;
  upd_reg d (Val.longofintu (Val.add d32 s32))
  .

Lemma bind_push_reset_cache : forall (st : state) s,
   eval_src32 s (reset_cache st) = (option_map (fun st => (fst st, reset_cache (snd st))) (eval_src32 s st)). 
Proof.
  destruct s. 
  unfold returnM. 
  destruct st.
  cbn. 
  unfold reset_cache. 
  cbn. 
  reflexivity. 
  unfold returnM. 
  reflexivity.
Qed.

Lemma cache_inv_eval : forall s st, option_map (fun output => cache (snd output)) (eval_src32 s st) = option_map (fun output => cache st) (eval_src32 s st).
Proof.
intros.
unfold eval_src32.
destruct s; [reflexivity | reflexivity].
Qed.

Lemma upd_reg_inv : forall st x d,
  option_map
  (fun z : unit * state => (fst z, upd_state_cache (snd z) (cache st))) (upd_reg d x (reset_cache st)) = (upd_reg d x st)
  .
Proof.
intros.
unfold upd_reg. 
destruct x; [ reflexivity | reflexivity | reflexivity |reflexivity |reflexivity |reflexivity ].
Qed.

Lemma step_alu_binary_operation_id : forall d s st, 
option_map (fun z => (fst z, (upd_state_cache (snd z) (cache st)))) 
            (step_alu_binary_operation_simpl d s (reset_cache st)) =
step_alu_binary_operation_simpl d s st.
Proof.
intros.
cbn.
rewrite map_bind_push.
rewrite bind_push_f.
2 : { apply bind_push_reset_cache. }
unfold bindM. 
assert (H := (cache_inv_eval s st )).
destruct (eval_src32 s st) as [ (x', st') | ] eqn:Heq  ; [ | reflexivity ].
cbn in H.
inversion H. 
rewrite upd_reg_inv.
f_equal.
Qed.

Lemma check_mem_equiv:
  forall p addr chunk st1' st1 st2 ptr
  (Hst: match_states st1 st1')
  (Hcheck: Semantics.check_mem p chunk addr false st1 = Some (ptr, st2)),
    exists st2',
      Semantics.check_mem p chunk addr true st1' = Some (ptr, st2') /\
      match_states st2 st2'.
Proof.
Admitted.

Definition getCache (st : option (val * state)) : ListNat.t :=
  match st with
  | None => []
  | Some (a, s) => cache s
  end.


Lemma check_state : forall m p addr chunk st,
option_map (fun s => snd s) (check_one_memory_region m p addr chunk st) = option_map (fun s => st) (check_one_memory_region m p addr chunk st). 
Proof.
intros.
unfold check_one_memory_region.
cbn.
destruct (compu_le
        (Val.add (Val.sub addr (start_addr m)) (memory_chunk_to_valu32 chunk))
        (block_size m) &&
      (compu_le (Val.sub addr (start_addr m))
         (memory_chunk_to_valu32_upbound chunk) &&
       match
         val32_modu (Val.sub addr (start_addr m))
           (memory_chunk_to_valu32 chunk)
       with
       | Vint n2 => Int.eq Int.zero n2
       | _ => false
       end) && perm_ge (block_perm m) p)%bool eqn:Hb.
+ reflexivity. 
+ reflexivity. 
Qed.

(*
Lemma check_state_mem : forall num id p addr chunk st,
option_map (fun s => snd s) (check_all_memory_regions num id p chunk addr b st) = option_map (fun s => st) (check_all_memory_regions num id p chunk addr b st). 
Proof.
intros.
unfold check_one_memory_region.
cbn.
destruct (compu_le
        (Val.add (Val.sub addr (start_addr m)) (memory_chunk_to_valu32 chunk))
        (block_size m) &&
      (compu_le (Val.sub addr (start_addr m))
         (memory_chunk_to_valu32_upbound chunk) &&
       match
         val32_modu (Val.sub addr (start_addr m))
           (memory_chunk_to_valu32 chunk)
       with
       | Vint n2 => Int.eq Int.zero n2
       | _ => false
       end) && perm_ge (block_perm m) p)%bool eqn:Hb.
+ reflexivity. 
+ reflexivity. 
Qed.
*)


Lemma check_mem_equiv2:
  forall p addr chunk st1 c
  (H : getCache (Semantics.check_mem p chunk addr true st1) = c)
  ,
      Semantics.check_mem p chunk addr true st1 = 
      option_map (fun st => (fst st, upd_state_cache (snd st) c)) (Semantics.check_mem p chunk addr false (reset_cache st1)).
Proof.
intros.
cbn.
unfold bindM at 1.
unfold eval_cache.
destruct (State.eval_cache st1) eqn:Heval_cache.
2 : { admit. }
destruct n eqn:Hn.
- unfold bindM at 1.
  unfold eval_mrs_num.
  unfold eval_mem_num.
  admit. 
- unfold bindM at 1. 
  unfold get_mem_region.
  simpl.
  destruct (n0 - 0 <? mrs_num st1)%nat eqn:Hcond1 ; [ | admit].
  destruct (nth_error (bpf_mrs st1) (n0 - 0)) eqn:Hcond2; [ | admit] .
  unfold bindM at 1.
  assert (Hcheck := (check_state m p addr chunk st1)).
  destruct (check_one_memory_region m p addr chunk st1) as [(x', st') | ] eqn:Hcheck_one; [ | admit] .
  cbn in Hcheck.
  inversion Hcheck.
  unfold cmp_ptr32_nullM in *.
  unfold bindM at 1.
  destruct (cmp_ptr32_null (State.eval_mem st1) x') eqn:Hcmp; [ | admit].
  destruct b eqn:Hb.
  -- unfold bindM, eval_mrs_num, eval_mem_num.
     admit. 
     (* todo *)
  -- unfold returnM.
     admit.
Admitted.

Lemma bind_push_get_mem_region_reset_cache : forall (st : state) num,
   get_mem_region num (reset_cache st) = (option_map (fun st => (fst st, reset_cache (snd st))) (get_mem_region num st)). 
Proof.
intros.
unfold get_mem_region.
cbn.
destruct (mrs_num st).
+ reflexivity.
+ destruct (num <=? n)%nat. 
  - destruct (nth_error (bpf_mrs st) num).
    * reflexivity. 
    * reflexivity. 
  - reflexivity.
Qed.


Lemma bind_push_check_one_mem_region_reset_cache : forall (st : state) mr p addr chunk,
check_one_memory_region mr p addr chunk (reset_cache st) =
option_map (fun st0 : val * state => (fst st0, reset_cache (snd st0)))
  (check_one_memory_region mr p addr chunk st).
Proof.
intros.
cbn.
destruct (compu_le
     (Val.add (Val.sub addr (start_addr mr)) (memory_chunk_to_valu32 chunk))
     (block_size mr) &&
   (compu_le (Val.sub addr (start_addr mr))
      (memory_chunk_to_valu32_upbound chunk) &&
    match
      val32_modu (Val.sub addr (start_addr mr))
        (memory_chunk_to_valu32 chunk)
    with
    | Vint n2 => Int.eq Int.zero n2
    | _ => false
    end) && perm_ge (block_perm mr) p)%bool eqn:Hb.
- reflexivity.
- reflexivity.
Qed.

Lemma check_all_mem_equiv : forall id p chunk addr st1 c num
  (H : getCache
      (check_all_memory_regions num id p chunk addr true st1) = c),
  check_all_memory_regions num id p chunk addr true st1 =
    option_map (fun st : val * state => (fst st, upd_state_cache (snd st) c))
  (check_all_memory_regions num id p chunk addr false (reset_cache st1)).
Proof.
intros.
cbn.
generalize H.
clear H.
generalize id.
clear id.
generalize st1.
clear st1.
induction num.
+ intros. 
  cbn. 
  unfold returnM. 
  cbn in H. 
  rewrite <- H.
  cbn.
  f_equal.
  f_equal.
  unfold upd_state_cache, reset_cache, cache. cbn. destruct st1. reflexivity.
+ intros.
  cbn.
  rewrite map_bind_push.
  rewrite bind_push_f.
  2 : { apply bind_push_get_mem_region_reset_cache. }
  f_equal.
  extensionality mr.
  extensionality st.
  destruct id eqn:Hid.
  - rewrite map_bind_push.
    rewrite bind_push_f.
    2 : { apply bind_push_check_one_mem_region_reset_cache. }
    f_equal.
    extensionality ptr.
    extensionality st0.
    rewrite map_bind_push.
    rewrite bind_push_f.
    2 : { admit. }
    f_equal.
    extensionality b.
    clear st0.
    extensionality st0.
    destruct b.
    * cbn. apply IHnum. admit.
    * admit. 
  - destruct (num =? n)%nat.
    * rewrite IHnum. 
      admit.
Admitted.


Lemma InvStep_alu : forall a bop d s st,
    (step_alu_binary_operation a bop d s) (ground_state_rs (regs_st st)) = 
    proj_rs ((step_alu_binary_operation a bop d s) st).
Proof.
intros.
cbn.
cbn.
destruct st. cbn.
unfold ground_state.
unfold step_alu_binary_operation.
cbn.
destruct a. cbn.

Lemma equiv_is_well_chunk_bool:
 forall st1 st2 st1' res a bop d s
 (H_sim : match_states st1 st2)
 (H_rel : Some (res, st1') = step_alu_binary_operation a bop d s st1),
  exists st2',
    Some (res, st2') = step_alu_binary_operation a bop d s st2 /\
    match_states st1' st2'.
Proof.
intros.
unfold step_alu_binary_operation.
unfold is_well_chunk_bool, returnM.
intros.
exists st2.
destruct a. cbn. inversion H_rel; subst; (split; [f_equal | assumption ]).
Qed.

Lemma cmp_ptr32_nullM_equiv:
 forall st1 st2 st1' res val
 (H_sim : match_states st1 st2)
 (H_rel : Some (res, st1') = cmp_ptr32_nullM val st1),
  exists st2',
    Some (res, st2') = cmp_ptr32_nullM val st2 /\
    match_states st1' st2'.
Proof.
  unfold cmp_ptr32_nullM, returnM; intros.

  assert (Heq: State.eval_mem st1 = State.eval_mem st2).
  { clear - H_sim.
    unfold match_states in *.
    unfold State.eval_mem.
    intuition.
  }

  rewrite <- Heq; clear Heq.
  exists st2.
  destruct rBPFValues.cmp_ptr32_null; inversion H_rel; subst.
  split; [f_equal | assumption].
Qed.

Lemma get_mem_region_equiv:
 forall st1 st2 res n
 (H_sim : match_states st1 st2)
 (H_rel : Some (res, st1) = get_mem_region n st1),
    Some (res, st2) = get_mem_region n st2 /\
    match_states st1 st2.
Proof.
  unfold get_mem_region, returnM; intros.

  assert (Heq: mrs_num st1 = mrs_num st2).
  { clear - H_sim.
    unfold match_states in *.
    intuition.
  }

  rewrite <- Heq; clear Heq.
  destruct (_ <? _)%nat; [| inversion H_rel].
  assert (Heq: bpf_mrs st1 = bpf_mrs st2).
  { clear - H_sim.
    unfold match_states in *.
    intuition.
  }
  rewrite <- Heq; clear Heq.

  destruct nth_error; inversion H_rel; subst.
  split; [f_equal | assumption].
Qed.

Lemma get_start_addr_equiv:
 forall st1 st2 st1' res mrs
 (H_sim : match_states st1 st2)
 (H_rel : Some (res, st1') = get_start_addr mrs st1),
  exists st2',
    Some (res, st2') = get_start_addr mrs st2 /\
    match_states st1' st2'.
Proof.
  unfold get_start_addr, returnM; intros.
  exists st2.
  inversion H_rel; subst.
  split; [f_equal | assumption].
Qed.

Lemma get_block_size_equiv:
 forall st1 st2 st1' res mrs
 (H_sim : match_states st1 st2)
 (H_rel : Some (res, st1') = get_block_size mrs st1),
  exists st2',
    Some (res, st2') = get_block_size mrs st2 /\
    match_states st1' st2'.
Proof.
  unfold get_block_size, returnM; intros.
  exists st2.
  inversion H_rel; subst.
  split; [f_equal | assumption].
Qed.

Lemma get_block_perm_equiv:
 forall st1 st2 st1' res mrs
 (H_sim : match_states st1 st2)
 (H_rel : Some (res, st1') = get_block_perm mrs st1),
  exists st2',
    Some (res, st2') = get_block_perm mrs st2 /\
    match_states st1' st2'.
Proof.
  unfold get_block_perm, returnM; intros.
  exists st2.
  inversion H_rel; subst.
  split; [f_equal | assumption].
Qed.

Lemma get_sub_equiv:
 forall st1 st2 st1' res a b
 (H_sim : match_states st1 st2)
 (H_rel : Some (res, st1') = get_sub a b st1),
  exists st2',
    Some (res, st2') = get_sub a b st2 /\
    match_states st1' st2'.
Proof.
  unfold get_sub, returnM; intros.
  exists st2.
  inversion H_rel; subst.
  split; [f_equal | assumption].
Qed.

Lemma get_add_equiv:
 forall st1 st2 st1' res a b
 (H_sim : match_states st1 st2)
 (H_rel : Some (res, st1') = get_add a b st1),
  exists st2',
    Some (res, st2') = get_add a b st2 /\
    match_states st1' st2'.
Proof.
  unfold get_add, returnM; intros.
  exists st2.
  inversion H_rel; subst.
  split; [f_equal | assumption].
Qed.

(*
Lemma check_mem_aux2_equiv:
 forall st1 st2 st1' mrs perm addr chunk res
 (H_sim : match_states st1 st2)
 (H_rel : Some (res, st1') = check_mem_aux2 mrs perm addr chunk st1),
  exists st2',
    Some (res, st2') = check_mem_aux2 mrs perm addr chunk st2 /\
    match_states st1' st2'.
 Proof.
  unfold check_mem_aux2, returnM; intros.

  forward_one_step H_rel get_start_addr get_start_addr_equiv.
  forward_one_step H_rel get_block_size get_block_size_equiv.
  forward_one_step H_rel get_block_perm get_block_perm_equiv.
  forward_one_step H_rel get_sub get_sub_equiv.
  forward_one_step H_rel get_add get_add_equiv.

  destruct ( _ && _)%bool; inversion H_rel; subst;
    exists st2; (split; [f_equal | assumption]).
Qed.
*)

Lemma equiv_mem_num:
  forall st1 st2
    (Hst: match_states st1 st2),
    eval_mem_num st1 = eval_mem_num st2.
Proof.
  unfold match_states, eval_mem_num.
  intros.
  destruct Hst as (_ & _ & _ & Hnum & _). 
  apply Hnum.
Qed.


Lemma get_mem_region_some:
  forall st res num st',
    Some (res, st') = get_mem_region num st ->
      st = st'.
Proof.
  unfold get_mem_region.
  intros.
  destruct (_ <? _)%nat; inversion H.
  destruct nth_error; inversion H.
  reflexivity.
Qed.

(*
Lemma check_mem_aux2_some:
  forall res1 perm val chunk st res st',
    Some (res, st') = check_mem_aux2 res1 perm val chunk st ->
     st = st'.
Proof.
  unfold check_mem_aux2.
  unfold get_start_addr, get_block_size, get_block_perm, get_sub, get_add.
  unfold bindM, returnM; intros.
  destruct ( _ && _)%bool; inversion H; reflexivity.
Qed.
*)

Lemma cmp_ptr32_nullM_some:
  forall st st' res v,
    Some (res, st') = cmp_ptr32_nullM v st ->
      st = st'.
Proof.
  unfold cmp_ptr32_nullM; intros.
  destruct rBPFValues.cmp_ptr32_null; inversion H; reflexivity.
Qed.


Lemma equiv_check_mem_aux:
  forall index st1 cache_id perm chunk val st2 res st1'

  (Hsame: forall t, exists l, upd_cache t st2 = Some (tt, l)) (**r this should be guarantee by step *)
  (Hcache_id_skip: exists res0, get_mem_region (cache_id - 1) st2 = Some (res0, st2) /\ check_mem_aux2 res0 perm val chunk st2 = Some (Vnullptr, st2))

  (H_sim : match_states st1 st2)
  (H_rel : Some (res, st1') = check_mem_aux index cache_id perm chunk val false st1),
    exists st2',
    Some (res, st2') = check_mem_aux index cache_id perm chunk val true st2 /\
    match_states st1' st2'.
Proof.
  induction index; simpl; intros.
  - clear Hsame.
    exists st2.
    unfold returnM in *.
    inversion H_rel; subst.
    split; [f_equal | assumption].
  - (*forward_one_step H_rel get_mem_region get_mem_region_equiv.*)
    unfold bindM at 1; unfold bindM at 1 in H_rel.
    unfold get_mem_region in *. 
    assert (Heq: mrs_num st2 = mrs_num st1).
    {
      clear - H_sim. unfold match_states in H_sim. intuition.
    }
    rewrite Heq; rename Heq into Hmatch.
    destruct (_ <? _)%nat eqn: Htmp in H_rel; inversion H_rel.
    rewrite Htmp; rename Htmp into Hcond.

    assert (Heq: bpf_mrs st2 = bpf_mrs st1).
    {
      clear - H_sim. unfold match_states in H_sim. intuition.
    }
    rewrite Heq; rename Heq into Hmatch_mrs.

    destruct nth_error eqn: Htmp in H_rel; inversion H_rel.
    rewrite Htmp; rename Htmp into Hmr.
    destruct cache_id.
    + (**r cache_id = 0 => cache_id doesn't exist *)
      forward_one_step H_rel check_mem_aux2 check_mem_aux2_equiv.
      apply check_mem_aux2_some in Heq.
      subst st3'.
      forward_one_step H_rel cmp_ptr32_nullM cmp_ptr32_nullM_equiv.
      apply cmp_ptr32_nullM_some in Heq; subst st3'.
      destruct res1.
      * (**r IH *) eapply IHindex; eauto.
      * (**r we prove that upd_cache doesn't change match_states *)
        unfold returnM in H_rel.
        inversion H_rel; subst.
        specialize (Hsame (S index)).
        destruct Hsame as (l & Hsame).
        clear - Hsame H_sim.
        unfold bindM, returnM.
        rewrite Hsame.
        eexists; split; [f_equal | ].
        unfold upd_cache, State.upd_cache in Hsame.
        destruct ListNat.assign; inversion Hsame.
        clear - H_sim.
        unfold match_states in *.
        simpl; intuition.
    + (**r cache <> 0 => cache exists *)
      destruct ( _ =? _)%nat eqn: Heq_cache.
      * (**r cache exists but check_mem already checked and return null *)
        rewrite Hmatch in *. 
        apply Nat.eqb_eq in Heq_cache.
        subst.
        simpl in Hcache_id_skip.
        rewrite Nat.sub_0_r in Hcache_id_skip.
        rewrite Hcond in Hcache_id_skip.
        destruct Hcache_id_skip as (res0 & Hnth_error & Hcheck).
        rewrite Hmatch_mrs in *.
        destruct nth_error eqn: Hnth; inversion Hnth_error; clear H1 H0. subst.
        inversion Hmr; subst.
        assert (Heq: check_mem_aux2 m perm val chunk st1 = Some (Vnullptr, st1)).
        {
        clear - H_sim Hcheck.
        unfold check_mem_aux2 in *.
        unfold get_start_addr, get_block_size, get_block_perm, get_sub, get_add in *.
        unfold bindM, returnM in *. 
        destruct (_ && _)%bool; inversion Hcheck; subst; f_equal.
        }
        unfold bindM at 1 in H_rel. 
        rewrite Heq in H_rel; clear Heq.
        unfold bindM at 1 in H_rel. 
        unfold cmp_ptr32_nullM in H_rel. unfold rBPFValues.cmp_ptr32_null in H_rel. 
        unfold Val.cmpu_bool in H_rel. unfold Vnullptr in H_rel. unfold Archi.ptr64 in H_rel. simpl in H_rel.
        rewrite Int.eq_true in H_rel.
        eapply IHindex; eauto.
        simpl. 
        rewrite Nat.sub_0_r.
        exists m.
        rewrite Hmatch. rewrite Hcond.
        rewrite Hmatch_mrs.
        rewrite Hnth. split; f_equal. assumption.
      * (**r cache hit *)
        forward_one_step H_rel check_mem_aux2 check_mem_aux2_equiv.
        apply check_mem_aux2_some in Heq.
        subst st3'.
        forward_one_step H_rel cmp_ptr32_nullM cmp_ptr32_nullM_equiv.
        apply cmp_ptr32_nullM_some in Heq; subst st3'.
        destruct res1.
        { (**r IH *) eapply IHindex; eauto. }
        { (**r we prove that upd_cache doesn't change match_states *)
          unfold returnM in H_rel.
          inversion H_rel; subst.
          specialize (Hsame (S index)).
          destruct Hsame as (l & Hsame).
          clear - Hsame H_sim.
          unfold bindM, returnM.
          rewrite Hsame.
          eexists; split; [f_equal | ].
          unfold upd_cache, State.upd_cache in Hsame.
          destruct ListNat.assign; inversion Hsame.
          clear - H_sim.
          unfold match_states in *.
          simpl; intuition. }
Qed.

Lemma mrs_num : forall st n
(Hnth : nth_error (cache st) (Z.to_nat (Int.signed (pc_loc st))) = Some (S n)), 
  (S n - 1 <? mrs_num st)%nat = true.
Proof.
Admitted.

Lemma mr_exists : forall st n
(Hnth : nth_error (cache st) (Z.to_nat (Int.signed (pc_loc st))) = Some (S n)), 
exists mr,
  nth_error (bpf_mrs st) (S n - 1)  = Some mr .
Proof.
Admitted.


Lemma equiv_check_mem : 
 forall res st1 st2 st1' perm ast val
 (H_sim : match_states st1 st2)
 (H_rel : Some (res, st1') = check_mem perm ast val false st1),
 exists st2', Some (res, st2') = check_mem perm ast val true st2 /\ match_states st1' st2'.
Proof.
intros.
unfold check_mem in *.
forward_one_step H_rel is_well_chunk_bool equiv_is_well_chunk_bool.
assert (H_eval : exists n, nth_error (cache st2) (Z.to_nat (Int.unsigned (pc_loc st2))) = Some n ).
admit.

destruct H_eval as (n & Hnth).
destruct res0.
2 : {  unfold returnM in *. inversion H_rel. exists st2. split; [f_equal | assumption]. }
unfold bindM at 1. unfold eval_cache. unfold State.eval_cache. cbn. unfold ListNat.index.  
rewrite Hnth.
destruct n.
  + 
    unfold bindM at 1. unfold bindM at 1 in H_rel.
    unfold eval_mrs_num in *.
    assert (Heq: eval_mem_num st1 = eval_mem_num st2).
    { 
    unfold match_states in H_sim.
    unfold eval_mem_num. intuition.
    }
    rewrite <- Heq. clear Heq.
    pose proof (equiv_check_mem_aux (eval_mem_num st1) st1 0 perm ast val st2 res st1') as Heq.
    destruct Heq.
    remember (Z.to_nat (Int.unsigned (pc_loc st2))) as index.
    unfold upd_cache. unfold State.upd_cache. rewrite <- Heqindex.
    assert (forall t, nth_error (cache st2) index = Some 0%nat -> exists new_cache, ListNat.assign (cache st2) index t = Some new_cache).
    {
    intros. clear H. destruct (cache st2).
      + destruct index. simpl in Hnth. discriminate Hnth.  simpl in Hnth. discriminate Hnth.
      + destruct index. simpl in Hnth. 
        - injection Hnth. intro. rewrite H.  simpl. exists (t :: t0). f_equal.
        - simpl. simpl in Hnth. simpl in IHt0. simpl.
    }
    - intro. unfold upd_cache. unfold State.upd_cache. destruct (cache st2).
      -- unfold nth_error in Hnth. destruct (Z.to_nat (Int.unsigned (pc_loc st2))); discriminate Hnth; discriminate Hnth. 
      -- simpl in Hnth. destruct (Z.to_nat (Int.unsigned (pc_loc st2))) as [ | index].
        ++ simpl in Hnth. injection Hnth; intro. rewrite H. simpl. exists  {|
      pc_loc := pc_loc st2;
      flag := flag st2;
      regs_st := regs_st st2;
      State.mrs_num := State.mrs_num st2;
      bpf_mrs := bpf_mrs st2;
      ins_len := ins_len st2;
      ins := ins st2;
      cache := t :: t0;
      bpf_m := bpf_m st2
    |}. f_equal.
        ++ cbn in Hnth. simpl. 
           assert (nth_error t0 index = Some 0%nat -> ListNat.assign t0 index t = 
        -- 
    - 
    simpl in Heq.
    rewrite <- (equiv_mem_num st1 st2); [ | apply H_sim].
    destruct check_mem_aux as [(res3, st3)| ] eqn:Htmp; [ | inversion H_rel].
    eapply equiv_check_mem_aux in H_sim; eauto.
    destruct H_sim as (st3', (Heq, H_sim)).
    rewrite <- Heq; clear Heq. clear Htmp.
    clear st1; rename st3 into st1.
    
    unfold bindM in *.
    destruct cmp_ptr32_nullM as [(res4, st3)| ] eqn:Htmp; [ | inversion H_rel].
    eapply cmp_ptr32_nullM_equiv in H_sim; eauto.
    destruct H_sim as (st4', (Heq, H_sim)).
    rewrite <- Heq; clear Heq.
    destruct res4; repeat ( unfold returnM in *; exists st4'; split; [ inversion H_rel; reflexivity | inversion H_rel; apply H_sim]).

  + unfold bindM at 1. unfold bindM at 1. unfold bindM at 1. unfold bindM at 1. 
    destruct (eval_mrs_regions st2) eqn: H. 
      2 : { inversion H. } 
      destruct p. unfold eval_mrs_regions in H. inversion H. rewrite <- H2. clear H2. clear H1. clear H.
      unfold get_mem_region. rewrite (mrs_num st2 n Hnth).
      unfold State.eval_mem_regions.
      destruct (mr_exists st2 n Hnth) as [mr Hmr].
      rewrite Hmr. 
      unfold check_mem_aux2.
      unfold bindM at 1. unfold get_start_addr. unfold returnM at 1.
      unfold bindM at 1. unfold get_block_size. unfold returnM at 1.
      unfold bindM at 1. unfold get_block_perm. unfold returnM at 1.
      unfold bindM at 1. unfold get_sub. unfold returnM at 1.
      unfold bindM at 1. unfold get_add. unfold returnM at 1.
      destruct ((rBPFValues.compu_le
   (Val.add (Val.sub val (MemRegion.start_addr mr))                
      (rBPFAST.memory_chunk_to_valu32 ast)) 
   (MemRegion.block_size mr) &&
 (rBPFValues.compu_le (Val.sub val (MemRegion.start_addr mr))
    (rBPFAST.memory_chunk_to_valu32_upbound ast) &&
  rBPFValues.comp_eq Vzero
    (rBPFValues.val32_modu (Val.sub val (MemRegion.start_addr mr))
       (rBPFAST.memory_chunk_to_valu32 ast))) &&
 rBPFMemType.perm_ge (MemRegion.block_perm mr) perm)%bool) eqn: Hcond.
      - clear Hcond. unfold returnM. admit.
      - clear Hcond.
        


      unfold check_mem_aux2.
      destruct (nth_error (State.eval_mem_regions st2) (S n - 1)) eqn : H.
      2 : { cbn in H. 

      
      2 : { 
      destruct (get_mem_region (S n - 1) (State.eval_mem_regions st2) st2) eqn: H. 
      ++  destruct p. unfold get_mem_region in H. 
      2 : { cbn in H.  }

    

Admitted.


(*
2 : { 
  - destruct n. 

  unfold eval_cache. unfold State.eval_cache. destruct (eval_cache st2) eqn : H.  destruct p. unfold eval

- unfold bindM at 1. unfold eval_cache. destruct (eval_cache st2) eqn : H_cache. 
  +  destruct p. cbn. destruct n; [cbn | cbn]. 
    -- cbn in H_rel.


unfold bindM at 1 in H_rel. 
destruct (SemanticsComm.is_well_chunk_bool ast st1) eqn: H. destruct p. cbn in H_rel.
destruct b. cbn. cbn in H_rel.
Admitted.
*)



Lemma equiv_steps : 
 forall res st1 st2 st1'
 (H_sim : match_states st1 st2)
 (H_rel : Some (res, st1') = step false st1),
 exists st2', Some (res, st2') = step true st2 /\ match_states st1' st2'.
Proof.
  intros.
  unfold Semantics.step, step in *.
Admitted.

Theorem equivalence_between_formal_and_opt_aux:
  forall f res st1 st2 st1'
    (H_sim : match_states st1 st2) 
    (H_rel : Some (res, st1') = bpf_interpreter_aux f false st1),
    exists st2', Some (res, st2') = bpf_interpreter_aux f true st2 /\ match_states st1' st2'.
Proof.  
  intros.
  induction f.
  - cbn in *. unfold upd_flag in *. injection H_rel. intros. exists (State.upd_flag Flag.BPF_ILLEGAL_LEN st2).
    split. f_equal. f_equal. apply H0. rewrite H. unfold match_states. cbn. admit.
  - cbn. cbn in H_rel.


  
  cbn.
Admitted.


Lemma upd_reg_sim:
  forall r v (res:unit) st1 st2 st1'
  (Hsim: match_states st1 st2)
  (Hinterp: Some (res, st1') = upd_reg r v st1),
    exists st2',
     Some (res, st2') = upd_reg r v st2 /\
     match_states st1' st2'.
Proof.
intros.
unfold upd_reg in *.
destruct v; inversion Hinterp.
subst res st1'.
exists (State.upd_reg r (Vlong i) st2).
split.
f_equal.
unfold match_states.
unfold State.upd_reg. cbn. unfold match_states in Hsim.
Admitted.

Lemma eval_reg_sim:
  forall r res st1 st2 st1'
  (Hsim: match_states st1 st2)
  (Hinterp: Some (res, st1') = eval_reg r st1),
    exists st2',
     Some (res, st2') = eval_reg r st2 /\
     match_states st1' st2'.
Proof.
Admitted.

(*
Lemma f_sim:
  forall A (res : A) st1 st2 st1' (f : M state A)
  (Hsim: match_states st1 st2)
  (Hinterp: Some (res, st1') = f st1)
  (HcacheInv : forall st1 st1' res, Some (res, st1') = f st1  -> cache st1' = cache st1),
     Some (res, st2) = f st2 /\
     match_states st1' st2'.
Proof.
intros.
destruct (f st1).

Admitted.
*)

Lemma eval_flag_sim:
  forall res st1 st2 st1'
  (Hsim: match_states st1 st2)
  (Hinterp: Some (res, st1') = eval_flag st1),
    exists st2',
     Some (res, st2') = eval_flag st2 /\
     match_states st1' st2'.
Proof.
Admitted.


Theorem equivalence_between_formal_and_opt:
  forall f input_v res st1 st2 st1'
  (Hsim: match_states st1 st2)
  (Hinterp: Some (res, st1') = bpf_interpreter f input_v false st1),
    exists st2',
     Some (res, st2') = bpf_interpreter f input_v true st2 /\
     match_states st1' st2'.
Proof.
  unfold bpf_interpreter.
  intros.
  unfold returnM.

  forward_one_step Hinterp upd_reg upd_reg_sim.

  forward_one_step Hinterp bpf_interpreter_aux equivalence_between_formal_and_opt_aux.

  forward_one_step Hinterp eval_flag eval_flag_sim.

  destruct Flag.flag_eq.
  { forward_one_step Hinterp eval_reg eval_reg_sim.

    injection Hinterp as Hres_eq Hst_eq.
    subst res st1'.
    eexists ;
    split; [f_equal | assumption].
  }

  injection Hinterp as Hres_eq Hst_eq.
  subst res st1'.
  eexists;
  split; [f_equal | assumption].
Qed.

(*
intros f input_v res st1 st2 st1'.
intros.
unfold bpf_interpreter.
unfold Semantics.bpf_interpreter in Hinterp.
unfold bindM at 1. 

unfold bindM, returnM. cbn.
destruct (bpf_interpreter f input_v st2) eqn: H.
2 : { cbn. inversion Hinterp.}
cbn.

  unfold Semantics.bpf_interpreter, SemanticsOpt.bpf_interpreter.
  intros.

  Ltac 2binds Hinterp := unfold bindM at 1; unfold bindM in Hinterp at 1.

  unfold bindM at 1.
  unfold bindM in Hinterp at 1.
  destruct upd_reg eqn: Htmp; [| inversion Hinterp].
  destruct p as (res3, st3).
  eapply upd_reg_sim in Hsim as Hsim2; eauto.
  clear Hsim; destruct Hsim2 as (st3' & Heq & Hsim).
  rewrite <- Heq; clear Heq Htmp.


  unfold bindM at 1.
  unfold bindM in Hinterp at 1.
  destruct Semantics.bpf_interpreter_aux eqn: Htmp; [| inversion Hinterp].
  destruct p as (res4, st4).
  eapply equivalence_between_formal_and_opt_aux in Hsim as Hsim2; eauto.
  clear Hsim; destruct Hsim2 as (st4' & Heq & Hsim).
  rewrite <- Heq; clear Heq Htmp.
*)

(**
If LTS is receptive and determinate then forward simulation for free: https://www.cl.cam.ac.uk/~pes20/CompCertTSO/doc/paper-long.pdf
**)

Close Scope monad_scope.
Close Scope Z_scope.

