From compcert Require Import Memory Memtype Integers Values Ctypes AST.
From Coq Require Import ZArith Lia List.

From bpf.comm Require Import Flag rBPFValues Regs BinrBPF MemRegion rBPFAST rBPFMemType ListAsArray.
From bpf.model Require Import Syntax Decode.
From bpf.model Require Import SemanticsSimpl.
Import ListNotations.


Lemma list_nth_error_assign_some:
  forall cache i v n,
    List.nth_error cache i = Some v ->
    exists cache', ListNat.assign cache i n = Some cache'.
Proof.
  induction cache; simpl; intros.
  {
    rewrite Coqlib.nth_error_nil in H.
    inversion H.
  }
  destruct i.
  - simpl in *.
    inversion H; subst.
    eexists; f_equal.
  - simpl in *.
    eapply IHcache with (n := n) in H.
    destruct H as (cache' & Heq).
    exists (a :: cache').
    rewrite Heq.
    f_equal.
Qed.

Lemma equivalence_between_formal_and_opt_check_all_memory_regions_cache_miss:
  forall n pc mrs_num cache_id cache chunk m0 addr mrs m ptr cache'
  (Hcache_miss: forall mrk,
      get_mem_region (Nat.sub cache_id 1%nat) mrs_num mrs = Some mrk ->
      cmp_ptr32_null m (check_one_memory_region mrk chunk addr m0) = Some true)
  (Hcache_well: List.nth_error cache (Z.to_nat (Int.unsigned pc)) = Some cache_id)
  (Hcheck: check_all_memory_regions pc n 0%nat mrs_num cache chunk m0 addr mrs m false = Some (ptr, cache')),
    exists cache1 : list nat,
      check_all_memory_regions pc n cache_id mrs_num cache chunk m0 addr mrs m true = Some (ptr, cache1).
Proof.
  induction n; intros.
  {
    simpl in *.
    inversion Hcheck; subst; eexists; f_equal.
  }
  simpl; simpl in Hcheck.
  destruct get_mem_region as [ cur_mr | ] eqn: Hmem_region in Hcheck; [| inversion Hcheck].
  rewrite Hmem_region.
  destruct cache_id.
  - (**r no cache *)
    destruct cmp_ptr32_null as [ check_ptr | ]; [| inversion Hcheck].
    destruct check_ptr.
    + eapply IHn; eauto.
    + eapply list_nth_error_assign_some with (n := (S n)) in Hcache_well; eauto.
      destruct Hcache_well as (cache1 & Heq).
      exists cache1; rewrite Heq.
      inversion Hcheck; subst; f_equal.
  - (**r cache exists *)
    destruct (n =? cache_id)%nat eqn: Hcache_id_eq.
    + (**r cache hint *)
      destruct cmp_ptr32_null as [ cond | ] eqn: Hnull; [| inversion Hcheck].
      destruct cond.
      * eapply IHn; eauto.
      * exfalso. (**r check_mem guarantees that this case is always False *)
        injection Hcheck as Hcheck Hcache_eq.
        subst cache.
        rewrite Nat.eqb_eq in Hcache_id_eq.
        subst cache_id.
        clear - Hcache_miss Hmem_region Hnull.
        simpl in Hcache_miss.
        rewrite Nat.sub_0_r in Hcache_miss.
        specialize (Hcache_miss _ Hmem_region).
        rewrite Hcache_miss in Hnull.
        inversion Hnull.
    + (**r cache missing *)
      destruct cmp_ptr32_null as [ check_ptr | ]; [| inversion Hcheck].
      destruct check_ptr.
      * eapply IHn; eauto.
      * eapply list_nth_error_assign_some with (n := (S n)) in Hcache_well; eauto.
        destruct Hcache_well as (cache1 & Heq).
        exists cache1; rewrite Heq.
        inversion Hcheck; subst; f_equal.
Qed.

Lemma equivalence_between_formal_and_opt_check_all_memory_regions_cache_no_exists:
  forall n pc mrs_num cache chunk m0 addr mrs m ptr cache'
  (Hcache_well: List.nth_error cache (Z.to_nat (Int.unsigned pc)) = Some 0%nat)
  (Hcheck: check_all_memory_regions pc n 0%nat mrs_num cache chunk m0 addr mrs m false = Some (ptr, cache')),
    exists cache1 : list nat,
      check_all_memory_regions pc n 0%nat mrs_num cache chunk m0 addr mrs m true = Some (ptr, cache1).
Proof.
  induction n; intros.
  {
    simpl in *.
    inversion Hcheck; subst; eexists; f_equal.
  }
  simpl; simpl in Hcheck.
  destruct get_mem_region as [ cur_mr | ] eqn: Hmem_region in Hcheck; [| inversion Hcheck].
  rewrite Hmem_region.
  (**r no cache *)
  destruct cmp_ptr32_null as [ check_ptr | ]; [| inversion Hcheck].
  destruct check_ptr.
  + eapply IHn; eauto.
  + eapply list_nth_error_assign_some with (n := (S n)) in Hcache_well; eauto.
    destruct Hcache_well as (cache1 & Heq).
    exists cache1; rewrite Heq.
    inversion Hcheck; subst; f_equal.
Qed.


(*
Definition memory_region_disjoint (m: mem) (mr1 mr2: memory_region): Prop :=
  (block_ptr mr1 <> block_ptr mr2) /\ (
  Val.cmpu_bool (Mem.valid_pointer m) Cle
    (Val.add (start_addr mr1) (block_size mr1)) (start_addr mr2) = Some true \/
  Val.cmpu_bool (Mem.valid_pointer m) Cle
    (Val.add (start_addr mr2) (block_size mr2)) (start_addr mr1) = Some true). *)

Definition memory_region_disjoint (mrs_num: nat) (mrs: MyMemRegionsType): Prop :=
  forall m i j mr_i mr_j p addr chunk,
      i <> j ->
      get_mem_region i mrs_num mrs = Some mr_i ->
      get_mem_region j mrs_num mrs = Some mr_j ->
      cmp_ptr32_null m (check_one_memory_region mr_i p addr chunk) = Some false ->
      cmp_ptr32_null m (check_one_memory_region mr_j p addr chunk) = Some true.
        (*memory_region_disjoint m mr_i mr_j)) *)

Lemma check_all_memory_regions_cache_hint:
  forall n pc mrs_num cache p chunk addr mrs m ptr cache' id cur_mr
  (Hn: (id < n  <= mrs_num)%nat )
  (Hget : get_mem_region id mrs_num mrs = Some cur_mr)
  (Hdisjoint : memory_region_disjoint mrs_num mrs)
  (Hcmp : cmp_ptr32_null m (check_one_memory_region cur_mr p addr chunk) = Some false)
  (Hcache_well : nth_error cache (Z.to_nat (Int.unsigned pc)) = Some (S id))
  (Hcheck : check_all_memory_regions pc n 0 mrs_num cache p chunk addr mrs m false = Some (ptr, cache')),
    (check_one_memory_region cur_mr p addr chunk, cache) = (ptr, cache).
Proof.
  unfold memory_region_disjoint.
  induction n; simpl; intros.
  {
    inversion Hn. inversion H.
  }

  assert (Hcond: (id = n \/ id < n)%nat) by lia.

  destruct Hcond.
  {
    subst n.
    rewrite Hget in Hcheck.
    rewrite Hcmp in Hcheck.
    inversion Hcheck; subst.
    f_equal.
  }

  eapply IHn with (mrs_num := mrs_num) (cache' := cache'); eauto.
  - lia.
  - (**r Hcheck goes to Some true *)
    destruct (get_mem_region n _ _) as [mr_n | ] eqn: Hget_n; [| inversion Hcheck].
    clear IHn.

    specialize (Hdisjoint m id n cur_mr mr_n).
    assert (Heq: id <> n) by lia.
    specialize (Hdisjoint p addr chunk Heq Hget Hget_n Hcmp); clear Heq.

    rewrite Hdisjoint in Hcheck.
    apply Hcheck.
Qed.


Lemma equivalence_between_formal_and_opt_check_mem:
  forall pc len cache p chunk addr mrs_num mrs m ptr cache'
  (Hcache_inv: forall pc cache, Int.cmpu Clt pc (Int.repr (Z.of_nat len)) = true /\
    exists id, List.nth_error cache (Z.to_nat (Int.unsigned pc)) = Some id /\
    (exists cur_mr, get_mem_region (id - 1) mrs_num mrs = Some cur_mr /\
    forall b p addr chunk m, cmp_ptr32_null m (check_one_memory_region cur_mr p addr chunk) = Some b))
  (Hdisjoint : memory_region_disjoint mrs_num mrs)
  (Hcheck: check_mem pc cache p chunk addr false mrs_num mrs m = Some (ptr, cache')),
    exists cache1, check_mem pc cache p chunk addr true mrs_num mrs m = Some (ptr, cache1).
Proof.
  unfold memory_region_disjoint.
  unfold check_mem; intros.
  specialize (Hcache_inv pc cache).
  destruct Hcache_inv as (_ & id & Hcache_well & Hcache_inv).
  rewrite Hcache_well.
  destruct id; simpl.
  - (**r cache no exists *)
    eapply equivalence_between_formal_and_opt_check_all_memory_regions_cache_no_exists; eauto.
  - (**r cache exists *)
    destruct Hcache_inv as (cur_mr & Hget & Hcmp).
    simpl in Hget.
    rewrite Nat.sub_0_r in *.
    rewrite Hget.
    destruct cmp_ptr32_null as [ check_ptr | ] eqn: HcmpC.
    2:{ specialize (Hcmp true p addr chunk m). rewrite HcmpC in Hcmp. inversion Hcmp. }
    clear Hcmp; destruct check_ptr.
    + (**r cache miss *)
      eapply equivalence_between_formal_and_opt_check_all_memory_regions_cache_miss; eauto.
      simpl; rewrite Nat.sub_0_r; intros mrk Hgetk.
      rewrite Hget in Hgetk; inversion Hgetk; subst.
      assumption.
    + (**r cache hint *)
      (**r here we need the mem_regions are disjoint info *)
      exists cache.
      f_equal.
      (**r `get_mem_region id mrs_num mrs = Some cur_mr` implies `id < mrs_num` *)
      eapply check_all_memory_regions_cache_hint in Hcheck; eauto.
      unfold get_mem_region in Hget.
      destruct ((_ <? _)%nat) eqn: Hcond; [| inversion Hget].
      rewrite Nat.ltb_lt in Hcond.
      lia.
Qed.

Lemma equivalence_between_formal_and_opt_step:
  forall pc cache l len rs mrs_num mrs m pc' rs' m' f'
  (Hcache_inv: forall pc cache, Int.cmpu Clt pc (Int.repr (Z.of_nat len)) = true /\
    exists id, List.nth_error cache (Z.to_nat (Int.unsigned pc)) = Some id /\
    (exists cur_mr, get_mem_region (id - 1) mrs_num mrs = Some cur_mr /\
    forall b p addr chunk m, cmp_ptr32_null m (check_one_memory_region cur_mr p addr chunk) = Some b))
  (Hdisjoint : memory_region_disjoint mrs_num mrs)
  (Hstep: step pc cache l len rs mrs_num mrs m false = Some (pc', rs', m', f', cache)),
    exists cache1, step pc cache l len rs mrs_num mrs m true = Some (pc', rs', m', f', cache1).
Proof.
  unfold step; intros.
  destruct eval_ins eqn: Hins; [| inversion Hstep].
  destruct (decode _) as [ ins| ]; [| inversion Hstep].
  destruct ins.
  - destruct a.
    + inversion Hstep; subst; eexists; f_equal.
    + inversion Hstep; subst; eexists; f_equal.
  - unfold step_alu_binary_operation in *.
    destruct a.
    + destruct b; try (
        inversion Hstep; subst; eexists; f_equal).
      * destruct (if comp_ne _ _ then _ else _) as [res | ]; [| inversion Hstep].
        destruct res.
        apply Hstep.
      * destruct (if compu_lt _ _ then _ else _) as [res | ]; [| inversion Hstep].
        destruct res.
        apply Hstep.
      * destruct (if compu_lt _ _ then _ else _) as [res | ]; [| inversion Hstep].
        destruct res.
        apply Hstep.
      * destruct (if comp_ne _ _ then _ else _) as [res | ]; [| inversion Hstep].
        destruct res.
        apply Hstep.
      * destruct (if compu_lt _ _ then _ else _) as [res | ]; [| inversion Hstep].
        destruct res.
        apply Hstep.
    + destruct b; try (
        inversion Hstep; subst; eexists; f_equal).
      * destruct (if compl_ne _ _ then _ else _) as [res | ]; [| inversion Hstep].
        destruct res.
        apply Hstep.
      * destruct (if complu_lt _ _ then _ else _) as [res | ]; [| inversion Hstep].
        destruct res.
        apply Hstep.
      * destruct (if complu_lt _ _ then _ else _) as [res | ]; [| inversion Hstep].
        destruct res.
        apply Hstep.
      * destruct (if compl_ne _ _ then _ else _) as [res | ]; [| inversion Hstep].
        destruct res.
        apply Hstep.
      * destruct (if complu_lt _ _ then _ else _) as [res | ]; [| inversion Hstep].
        destruct res.
        apply Hstep.
  - inversion Hstep; subst; eexists; f_equal.
  - destruct step_branch_cond;
    inversion Hstep; subst; eexists; f_equal.
  - inversion Hstep; subst; eexists; f_equal.
  - inversion Hstep; subst; eexists; f_equal.
  - unfold step_load_x_operation in *.
    destruct check_mem as [(ptr & cache1) | ] eqn: Hcheck in Hstep; [| inversion Hstep].
    eapply equivalence_between_formal_and_opt_check_mem in Hcheck; eauto.
    destruct Hcheck as (cache2 & Hcheck).
    rewrite Hcheck.
    destruct cmp_ptr32_null as [check_ptr | ] eqn: Hcmp in Hstep; [| inversion Hstep].
    rewrite Hcmp.
    destruct check_ptr.
    + inversion Hstep; subst; eexists; f_equal.
    + destruct load_mem as [ res_v | ]; [| inversion Hstep].
      inversion Hstep; subst; eexists; f_equal.
  - unfold step_store_operation in *.
    destruct s.
    + destruct check_mem as [(ptr & cache1) | ] eqn: Hcheck in Hstep; [| inversion Hstep].
      eapply equivalence_between_formal_and_opt_check_mem in Hcheck; eauto.
      destruct Hcheck as (cache2 & Hcheck).
      rewrite Hcheck.
      destruct cmp_ptr32_null as [check_ptr | ] eqn: Hcmp in Hstep; [| inversion Hstep].
      rewrite Hcmp.
      destruct check_ptr.
      * inversion Hstep; subst; eexists; f_equal.
      * destruct store_mem_reg as [ res_v | ]; [| inversion Hstep].
        inversion Hstep; subst; eexists; f_equal.
    + destruct check_mem as [(ptr & cache1) | ] eqn: Hcheck in Hstep; [| inversion Hstep].
      eapply equivalence_between_formal_and_opt_check_mem in Hcheck; eauto.
      destruct Hcheck as (cache2 & Hcheck).
      rewrite Hcheck.
      destruct cmp_ptr32_null as [check_ptr | ] eqn: Hcmp in Hstep; [| inversion Hstep].
      rewrite Hcmp.
      destruct check_ptr.
      * inversion Hstep; subst; eexists; f_equal.
      * destruct store_mem_imm as [ res_v | ]; [| inversion Hstep].
        inversion Hstep; subst; eexists; f_equal.
  - destruct cmp_ptr32_null as [ ptr | ]; [| inversion Hstep].
    destruct ptr; inversion Hstep; subst; eexists; f_equal.
  - inversion Hstep; subst; eexists; f_equal.
  - inversion Hstep; subst; eexists; f_equal.
Qed.

Lemma check_all_memory_regions_disable_opt_same_cache:
  forall n pc cache p chunk addr mrs_num mrs m ptr cache1
  (Hcheck: check_all_memory_regions pc n 0 mrs_num cache p chunk addr mrs m false = Some (ptr, cache1)),
    cache = cache1.
Proof.
  induction n; simpl; intros.
  { inversion Hcheck; subst; simpl; auto. }
  destruct get_mem_region; [| inversion Hcheck].
  destruct cmp_ptr32_null; [| inversion Hcheck].
  destruct b.
  - eapply IHn; eauto.
  - inversion Hcheck; subst; simpl; auto.
Qed.

Lemma check_mem_disable_opt_same_cache:
  forall pc cache p chunk addr mrs_num mrs m ptr cache1
  (Hcheck: check_mem pc cache p chunk addr false mrs_num mrs m = Some (ptr, cache1)),
    cache = cache1.
Proof.
  unfold check_mem.
  intros.
  eapply check_all_memory_regions_disable_opt_same_cache; eauto.
Qed.

Lemma step_disable_opt_same_cache:
  forall pc cache l len rs mrs_num mrs m pc1 rs1 m1 f1 cache1
  (Hstep : step pc cache l len rs mrs_num mrs m false = Some (pc1, rs1, m1, f1, cache1)),
    cache = cache1.
Proof.
  unfold step, eval_ins; intros.
  destruct Int.cmpu; [| inversion Hstep].
  destruct nth_error; [| inversion Hstep].
  destruct decode; [| inversion Hstep].
  destruct b.
  - destruct a; inversion Hstep; subst; simpl; auto.
  - unfold step_alu_binary_operation in Hstep.
    destruct a.
    + destruct b; try (inversion Hstep; subst; simpl; auto).
      * destruct comp_ne.
        { destruct Val.divu; [| inversion Hstep]. inversion Hstep; subst; simpl; auto. }
        inversion Hstep; subst; simpl; auto.
      * destruct compu_lt; inversion Hstep; subst; simpl; auto.
      * destruct compu_lt; inversion Hstep; subst; simpl; auto.
      * destruct comp_ne.
        { destruct Val.modu; [| inversion Hstep]. inversion Hstep; subst; simpl; auto. }
        inversion Hstep; subst; simpl; auto.
      * destruct compu_lt; inversion Hstep; subst; simpl; auto.
    + destruct b; try (inversion Hstep; subst; simpl; auto).
      * destruct compl_ne.
        { destruct Val.divlu; [| inversion Hstep]. inversion Hstep; subst; simpl; auto. }
        inversion Hstep; subst; simpl; auto.
      * destruct complu_lt; inversion Hstep; subst; simpl; auto.
      * destruct complu_lt; inversion Hstep; subst; simpl; auto.
      * destruct compl_ne.
        { destruct Val.modlu; [| inversion Hstep]. inversion Hstep; subst; simpl; auto. }
        inversion Hstep; subst; simpl; auto.
      * destruct complu_lt; inversion Hstep; subst; simpl; auto.
  - inversion Hstep; subst; simpl; auto.
  - destruct step_branch_cond; inversion Hstep; subst; simpl; auto.
  - inversion Hstep; subst; simpl; auto.
  - inversion Hstep; subst; simpl; auto.
  - unfold step_load_x_operation in Hstep.
    destruct check_mem as [ (ptr & cache2) | ] eqn: Hcheck; [| inversion Hstep].
    eapply check_mem_disable_opt_same_cache in Hcheck; eauto.
    subst cache2.
    destruct cmp_ptr32_null; [| inversion Hstep].
    destruct b.
    + inversion Hstep; subst; simpl; auto.
    + destruct load_mem; inversion Hstep; subst; simpl; auto.
  - unfold step_store_operation in Hstep.
    destruct s.
    + destruct check_mem as [ (ptr & cache2) | ] eqn: Hcheck; [| inversion Hstep].
      eapply check_mem_disable_opt_same_cache in Hcheck; eauto.
      subst cache2.
      destruct cmp_ptr32_null; [| inversion Hstep].
      destruct b.
      * inversion Hstep; subst; simpl; auto.
      * destruct store_mem_reg; inversion Hstep; subst; simpl; auto.
    + destruct check_mem as [ (ptr & cache2) | ] eqn: Hcheck; [| inversion Hstep].
      eapply check_mem_disable_opt_same_cache in Hcheck; eauto.
      subst cache2.
      destruct cmp_ptr32_null; [| inversion Hstep].
      destruct b.
      * inversion Hstep; subst; simpl; auto.
      * destruct store_mem_imm; inversion Hstep; subst; simpl; auto.
  - destruct cmp_ptr32_null; [| inversion Hstep].
    destruct b; inversion Hstep; subst; simpl; auto.
  - inversion Hstep; subst; simpl; auto.
  - inversion Hstep; subst; simpl; auto.
Qed.

Lemma bpf_interpreter_aux_disable_opt_same_cache:
  forall fuel pc cache l len rs mrs_num mrs m pc1 rs1 m1 f1 cache1
  (Haux : bpf_interpreter_aux fuel pc cache l len rs mrs_num mrs m false = Some (pc1, rs1, m1, f1, cache1)),
    cache = cache1.
Proof.
  induction fuel; simpl; intros.
  { inversion Haux; subst; simpl; auto. }

  destruct Int.ltu.
  - destruct step as [((((pc2 & rs2) & m2) & f2) & cache')|] eqn: Hstep; [| inversion Haux].
    eapply step_disable_opt_same_cache in Hstep; eauto.
    subst cache'.
    destruct flag_eq.
    + destruct Int.ltu.
      * eapply IHfuel; eauto.
      * inversion Haux; subst; simpl; auto.
    + inversion Haux; subst; simpl; auto.
  - inversion Haux; subst; simpl; auto.
Qed.

Lemma check_all_memory_regions_simpl_cache:
  forall n pc cache p chunk addr mrs_num mrs m1 ptr cache1
  (Hcheck : check_all_memory_regions pc n 0 mrs_num cache p chunk addr mrs m1 false = Some (ptr, cache)),
    check_all_memory_regions pc n 0 mrs_num cache1 p chunk addr mrs m1 false = Some (ptr, cache1).
Proof.
  induction n; simpl; intros.
  { inversion Hcheck; subst; simpl; f_equal. }
  destruct get_mem_region; [| inversion Hcheck].
  destruct cmp_ptr32_null; [| inversion Hcheck].
  destruct b.
  - eapply IHn; eauto.
  - inversion Hcheck; subst; simpl; f_equal.
Qed.

Lemma check_mem_simpl_cache:
  forall pc cache p chunk addr mrs_num mrs m1 ptr cache1
  (Hcheck : check_mem pc cache p chunk addr false mrs_num mrs m1 = Some (ptr, cache)),
    check_mem pc cache1 p chunk addr false mrs_num mrs m1 = Some (ptr, cache1).
Proof.
  unfold check_mem; intros; simpl in *.
  eapply check_all_memory_regions_simpl_cache; eauto.
Qed.

Lemma step_simpl_cache:
  forall cache l len rs1 pc1 mrs_num mrs m1 pc' rs' m' f' cache2
  (Hstep: step (Int.add pc1 Int.one) cache l len rs1 mrs_num mrs m1 false = Some (pc', rs', m', f', cache)),
    step (Int.add pc1 Int.one) cache2 l len rs1 mrs_num mrs m1 false = Some (pc', rs', m', f', cache2).
Proof.
  unfold step; intros.
  destruct eval_ins; [| inversion Hstep].
  destruct decode; [| inversion Hstep].
  destruct b.
  - destruct a.
    + inversion Hstep; subst; simpl; f_equal.
    + inversion Hstep; subst; simpl; f_equal.
  - unfold step_alu_binary_operation in *.
    destruct a.
    + destruct b; try (inversion Hstep; subst; simpl; f_equal).
      * destruct comp_ne.
        { destruct Val.divu; inversion Hstep; subst; simpl; f_equal. }
        inversion Hstep; subst; simpl; f_equal.
      * destruct compu_lt; inversion Hstep; subst; simpl; f_equal.
      * destruct compu_lt; inversion Hstep; subst; simpl; f_equal.
      * destruct comp_ne.
        { destruct Val.modu; inversion Hstep; subst; simpl; f_equal. }
        inversion Hstep; subst; simpl; f_equal.
      * destruct compu_lt; inversion Hstep; subst; simpl; f_equal.
    + destruct b; try (inversion Hstep; subst; simpl; f_equal).
      * destruct compl_ne.
        { destruct Val.divlu; inversion Hstep; subst; simpl; f_equal. }
        inversion Hstep; subst; simpl; f_equal.
      * destruct complu_lt; inversion Hstep; subst; simpl; f_equal.
      * destruct complu_lt; inversion Hstep; subst; simpl; f_equal.
      * destruct compl_ne.
        { destruct Val.modlu; inversion Hstep; subst; simpl; f_equal. }
        inversion Hstep; subst; simpl; f_equal.
      * destruct complu_lt; inversion Hstep; subst; simpl; f_equal.
  - inversion Hstep; subst; simpl; f_equal.
  - destruct step_branch_cond; inversion Hstep; subst; simpl; f_equal.
  - inversion Hstep; subst; simpl; f_equal.
  - inversion Hstep; subst; simpl; f_equal.
  - unfold step_load_x_operation in *.
    destruct check_mem as [(ptr & cachek)|] eqn: Hcheck in Hstep; [| inversion Hstep].
    eapply check_mem_disable_opt_same_cache in Hcheck as Heq.
    subst cachek.
    eapply check_mem_simpl_cache in Hcheck; eauto.
    rewrite Hcheck.
    destruct cmp_ptr32_null; [| inversion Hstep].
    destruct b.
    + inversion Hstep; subst; simpl; f_equal.
    + destruct load_mem; inversion Hstep; subst; simpl; f_equal.
  - unfold step_store_operation in *.
    destruct s.
    + destruct check_mem as [(ptr & cachek)|] eqn: Hcheck in Hstep; [| inversion Hstep].
      eapply check_mem_disable_opt_same_cache in Hcheck as Heq.
      subst cachek.
      eapply check_mem_simpl_cache in Hcheck; eauto.
      rewrite Hcheck.
      destruct cmp_ptr32_null; [| inversion Hstep].
      destruct b.
      * inversion Hstep; subst; simpl; f_equal.
      * destruct store_mem_reg; inversion Hstep; subst; simpl; f_equal.
    + destruct check_mem as [(ptr & cachek)|] eqn: Hcheck in Hstep; [| inversion Hstep].
      eapply check_mem_disable_opt_same_cache in Hcheck as Heq.
      subst cachek.
      eapply check_mem_simpl_cache in Hcheck; eauto.
      rewrite Hcheck.
      destruct cmp_ptr32_null; [| inversion Hstep].
      destruct b.
      * inversion Hstep; subst; simpl; f_equal.
      * destruct store_mem_imm; inversion Hstep; subst; simpl; f_equal.
  - destruct cmp_ptr32_null; [| inversion Hstep].
    destruct b; inversion Hstep; subst; simpl; f_equal.
  - inversion Hstep; subst; simpl; f_equal.
  - inversion Hstep; subst; simpl; f_equal.
Qed.

Lemma bpf_interpreter_aux_simpl_cache:
  forall fuel cache l len rs1 pc1 mrs_num mrs m1 pc' rs' m' f' cache2
  (Haux : bpf_interpreter_aux fuel (Int.add pc1 Int.one) cache l len rs1 mrs_num mrs m1 false =
       Some (pc', rs', m', f', cache)),
    bpf_interpreter_aux fuel (Int.add pc1 Int.one) cache2 l len rs1 mrs_num mrs m1 false =
      Some (pc', rs', m', f', cache2).
Proof.
  induction fuel; simpl; intros.
  { inversion Haux; subst; simpl; f_equal. }

  destruct Int.ltu; [| inversion Haux].
  destruct step as [((((pc2 & rs2) & m2) & f2) & cache')|] eqn: Hstep in Haux; [| inversion Haux].
  eapply step_disable_opt_same_cache in Hstep as Heq.
  subst cache'.
  eapply step_simpl_cache in Hstep; eauto.
  2:{ f_equal. }
  rewrite Hstep.
  destruct flag_eq.
  - destruct Int.ltu.
    + eapply IHfuel; eauto.
    + inversion Haux; subst; simpl; f_equal.
  - inversion Haux; subst; simpl; f_equal.
Qed.


Lemma equivalence_between_formal_and_opt_bpf_interpreter_aux:
  forall fuel pc cache l len rs mrs_num mrs m pc' rs' m' f'
  (Hcache_inv: forall pc cache, Int.cmpu Clt pc (Int.repr (Z.of_nat len)) = true /\
    exists id, List.nth_error cache (Z.to_nat (Int.unsigned pc)) = Some id /\
    (exists cur_mr, get_mem_region (id - 1) mrs_num mrs = Some cur_mr /\
    forall b p addr chunk m, cmp_ptr32_null m (check_one_memory_region cur_mr p addr chunk) = Some b))
  (Hdisjoint : memory_region_disjoint mrs_num mrs)
  (Haux: bpf_interpreter_aux fuel pc cache l len rs mrs_num mrs m false = Some (pc', rs', m', f', cache)),
    exists cache1, bpf_interpreter_aux fuel pc cache l len rs mrs_num mrs m true = Some (pc', rs', m', f', cache1).
Proof.
  induction fuel; intros.
  - simpl in *.
    inversion Haux; subst.
    eexists; f_equal.
  - simpl in Haux.
    simpl.
    destruct Int.ltu; [| inversion Haux; subst; eexists; f_equal].
    destruct step as [((((pc1 & rs1) & m1) & f1) & cache1) | ] eqn: Hstep; [| inversion Haux].

    (* prove cache and cache1 are same *)
    eapply step_disable_opt_same_cache in Hstep as Heq.
    subst cache1.

    eapply equivalence_between_formal_and_opt_step in Hstep as Hstep_simpl; eauto.

    destruct Hstep_simpl as (cache2 & Hstep_simpl).
    rewrite Hstep_simpl.
    destruct flag_eq.
    + destruct Int.ltu.
      * eapply IHfuel; eauto.
        eapply bpf_interpreter_aux_simpl_cache; eauto.
      * inversion Haux; subst; eexists; f_equal.
    + inversion Haux; subst; eexists; f_equal.
Qed.

Theorem equivalence_between_formal_and_opt:
  forall (fuel: nat) (ctx_ptr: val) (pc: int) (cache: list nat) (l: list int64) (len: nat)
  (rs: regmap) (mrs_num: nat) (mrs: MyMemRegionsType) (m: mem) res pc' rs' m' f'
  (Hcache_inv: forall pc cache, Int.cmpu Clt pc (Int.repr (Z.of_nat len)) = true /\
    exists id, List.nth_error cache (Z.to_nat (Int.unsigned pc)) = Some id /\
    (exists cur_mr, get_mem_region (id - 1) mrs_num mrs = Some cur_mr /\
    forall b p addr chunk m, cmp_ptr32_null m (check_one_memory_region cur_mr p addr chunk) = Some b))
  (Hdisjoint : memory_region_disjoint mrs_num mrs)
  (Hinterp: bpf_interpreter fuel ctx_ptr pc cache l len rs mrs_num mrs m false = Some (res, pc', rs', m', f', cache)),
  exists cache1,
    bpf_interpreter fuel ctx_ptr pc cache l len rs mrs_num mrs m true = Some (res, pc', rs', m', f', cache1).
Proof.
  unfold bpf_interpreter.
  intros.
  destruct bpf_interpreter_aux as [ ((((pc1 & rs1) & m1) & f1) & cache1)| ] eqn: Haux; [| inversion Hinterp].
  eapply bpf_interpreter_aux_disable_opt_same_cache in Haux as Heq; subst cache1.
  eapply equivalence_between_formal_and_opt_bpf_interpreter_aux in Haux; eauto.
  destruct Haux as (cache1 & Haux); rewrite Haux.
  destruct flag_eq; inversion Hinterp; subst; eexists; f_equal.
Qed.