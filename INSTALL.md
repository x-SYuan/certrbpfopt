
# Build world: opam

_NB: you may need to add `sudo` everywhere_

Please don't hesitate to contact [me](https://shenghaoyuan.github.io/) as the install is not easy.

1. basic enviroment for experiments (you can skip it)
```shell
# assume your ubuntu is named `bob`
# current folder: /home/bob/CertrBPF
# install the basic tool
apt-get update && apt-get install -y software-properties-common

# install gcc 9.4.0
apt install build-essential

# install gcc lib
apt-get install gcc-multilib

# install llvm
apt-get install -y llvm
apt-get install clang

# install pip3 and pyelftools
apt install python3-pip python3-serial
pip3 install pyelftools
```

2. OPAM
```shell
# install opam
add-apt-repository ppa:avsm/ppa
apt update
apt install opam

# install ocaml+coq+compcert etc by opam
opam init
# install ocaml
opam switch create bpf ocaml-base-compiler.4.11.1

eval $(opam env)

# It is better to close all your terminals and reopen them to flash the enviroment, Important !!!

opam switch list
#   switch  compiler      description
->  bpf     ocaml.4.11.1  bpf

# install coq, compcert-32, etc.
opam repository add coq-released https://coq.inria.fr/opam/released

# install coq etc (optional: and coqide)
opam install coq.8.13.2 coq-elpi.1.11.0 coqide.8.13.2

# optional: install headache
# `opam install depext`
# `opam depext headache`
# `opam install headache`

```
# Build compcert
There are two ways to install compcert:

1. using opam to install compcert (not so recommand because we install the large coq-vst but just use clightgen)

```shell
# NB we need to keep the build directory of compcert-32 by `-b`
opam install -b coq-compcert-32.3.11
# install cligthgen
opam install coq-vst-32.2.9 # or 2.8
```

2. compile compcert from source code
```shell
# download source code from https://github.com/AbsInt/CompCert/releases/tag/v3.11
# The latest compcert.3.12 should also be ok
wget https://github.com/AbsInt/CompCert/archive/refs/tags/v3.11.zip
# make sure `YOUR_COMPCERT_DIR` is empty as dx requires `YOUR_COMPCERT_DIR/` has only one folder named `compcert`
# unzip source code to `YOUR_COMPCERT_DIR` and rename the unzip folder to `compcert` (it is very important!!!)
cd YOUR_COMPCERT_DIR/compcert

# install coq-flocq.4.1.0
opam install coq-flocq.4.1.0
# install 32-bit compcert
./configure arm-linux -use-external-Flocq -clightgen
# you may should install menhir
# my version `menhir.20210419`
opam install menhir
# `make all` will return `make runtime` error if the target platform is not ARM, so just do `make ccomp`.
# We use `arm-linux` to compile the CompCert ARM backend because we have a JIT compiler target ARM, and later we hope to reuse this formal ARM semantics to prove JIT correct.
# if `make ccomp` returns xxx.v make inconsistency with the assumption ... Please uses the `v3.11.zip`
make ccomp
make clightgen
```

# Build coq2html (optional)

```shell
# install coq2html if you hope to generate CertrBPF Coq development document with the html form
git clone https://github.com/xavierleroy/coq2html.git
cd coq2html
make coq2html
```

# Set path

```shell
# set path variables & add COQPATH
apt-get install vim
# 1) find . -name ".opam" -type d
#  ==> ./home/bob/.opam
# 2); adding the line: `export PATH=$PATH:/home/bob/.opam/bpf/bin:/home/bob/CertrBPF/coq2html`
# 3); adding the line `export COQPATH="$(opam var lib)/coq-variant/compcert32"` when selecting opam to install compcert
#     adding the line `export COQPATH="YOUR_COMPCERT_DIR"` when selecting to install compcert from source code.
#Important: if you recompile CompCert again, remember to comment COQPATH firstly!!!
vim /home/bob/.bashrc
source /home/bob/.bashrc
```

# Build dx
```shell
# current folder: /home/bob/CertrBPF
# install dx
# the current main brach support compcert.3.11 (2022-08-25)
git clone https://gitlab.univ-lille.fr/samuel.hym/dx.git
cd dx
# when using opam to install compcert-32
./configure --cprinterdir="$(opam var lib)/dx" --compcertdir="$(opam var coq-compcert-32:build)" --install-compcert-printer
# when using source code
./configure --cprinterdir="$(opam var lib)/dx" --compcertdir="YOUR_COMPCERT_DIR/compcert" --install-compcert-printer
make all install
# if you get warning such as xxx is remapped into dx.xxx, which means that `YOUR_COMPCERT_DIR` has two or more folders, please move them
# if you get warning about compcert.xxx, which represents that you forget set `COQPATH`
```
# Build CertrBPF
```shell
# current folder: /home/bob/CertrBPF/dx
# install CertrBPF
cd ..
# possible branches: CertrBPF-Opt / rbpf32 / jit / CAV22-AE
git clone --branch xxx https://gitlab.inria.fr/syuan/rbpf-dx.git
cd rbpf-dx

# when using source code
./configure --opamprefixdir=/home/bob/.opam/bpf --compcertdir=YOUR_COMPCERT_DIR/compcert

make all
```
# Test CertrBPF 
```shell
# current folder: /home/bob/CertrBPF/rbpf-dx
# Here, we only test the native board (*You could reproduce the same result in the paper if you have the same physical boards: Nordic nRF52840 development kit, the Espressif WROOM-32 board, and the Sipeed Longan Nano GD32VF103CBT6 development board*)

# test bench_bpf_coq_incr
# compile CertBPF
make -C benchmark_data/bench_bpf_coq_incr/bpf
make -C benchmark_data/bench_bpf_coq_incr
# run on a native board using CertBPF
make -C benchmark_data/bench_bpf_coq_incr term
# complie original rBPF: Vanilla-rBPF
make -C benchmark_data/bench_bpf_coq_incr BPF_COQ=0 BPF_USE_JUMPTABLE=0
make -C benchmark_data/bench_bpf_coq_incr BPF_COQ=0 BPF_USE_JUMPTABLE=0 term

# test bench_bpf_coq_unit
# compile CertBPF
make -C benchmark_data/bench_bpf_coq_unit
make -C benchmark_data/bench_bpf_coq_unit term
# complie original rBPF: Vanilla-rBPF
make -C benchmark_data/bench_bpf_coq_unit BPF_COQ=0 BPF_USE_JUMPTABLE=0
make -C benchmark_data/bench_bpf_coq_unit BPF_COQ=0 BPF_USE_JUMPTABLE=0 term
```
