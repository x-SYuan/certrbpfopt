(**************************************************************************)
(*  This file is part of CertrBPF,                                        *)
(*  a formally verified rBPF verifier + interpreter + JIT in Coq.         *)
(*                                                                        *)
(*  Copyright (C) 2022 Inria                                              *)
(*                                                                        *)
(*  This program is free software; you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation; either version 2 of the License, or     *)
(*  (at your option) any later version.                                   *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(**************************************************************************)

From compcert Require Import Memory Memtype Integers Values Ctypes AST.
From Coq Require Import ZArith Lia.

From bpf.comm Require Import Flag rBPFValues Regs BinrBPF MemRegion rBPFAST rBPFMemType ListAsArray.
From bpf.model Require Import Syntax Decode.

Open Scope Z_scope.

Definition eval_src (s:reg+imm) (rs: regmap): val :=
  match s with
  | inl r => eval_regmap r rs
  | inr i => Val.longofint (sint32_to_vint i)
  end.

Definition eval_reg32 (r:reg) (rs: regmap): val :=
  val_intuoflongu (eval_regmap r rs).

Definition eval_src32 (s:reg+imm) (rs: regmap): val :=
  match s with
  | inl r => eval_reg32 r rs
  | inr i => sint32_to_vint i
  end.
Close Scope Z_scope.

Definition step_alu_binary_operation (a: arch) (bop: binOp) (d :reg) (s: reg+imm) (rs: regmap):
  option (regmap * bpf_flag) :=
  match a with
  | A32 =>
    let d32 := eval_reg32 d rs in
    let s32 := eval_src32 s rs in
      match bop with
      | BPF_ADD  => Some (upd_regmap d (Val.longofintu (Val.add  d32 s32)) rs, BPF_OK)
      | BPF_SUB  => Some (upd_regmap d (Val.longofintu (Val.sub  d32 s32)) rs, BPF_OK)
      | BPF_MUL  => Some (upd_regmap d (Val.longofintu (Val.mul  d32 s32)) rs, BPF_OK)
      | BPF_DIV  => if comp_ne s32 Vzero then
                      match Val.divu d32 s32 with
                      | Some res => Some (upd_regmap d (Val.longofintu res) rs, BPF_OK)
                      | None     => None
                      end
                    else
                      Some (rs, BPF_ILLEGAL_DIV)
      | BPF_OR   => Some (upd_regmap d (Val.longofintu (Val.or   d32 s32)) rs, BPF_OK)
      | BPF_AND  => Some (upd_regmap d (Val.longofintu (Val.and  d32 s32)) rs, BPF_OK)
      | BPF_LSH  => if compu_lt s32 (Vint (Int.repr 32)) then
                      Some (upd_regmap d (Val.longofintu (Val.shl d32 s32)) rs, BPF_OK)
                    else
                      Some (rs, BPF_ILLEGAL_SHIFT)

      | BPF_RSH  => if compu_lt s32 (Vint (Int.repr 32)) then
                      Some (upd_regmap d (Val.longofintu (Val.shru d32 s32)) rs, BPF_OK)
                    else
                      Some (rs, BPF_ILLEGAL_SHIFT)

      | BPF_MOD  => if comp_ne s32 Vzero then (** run-time checking *)
                      match Val.modu d32 s32 with
                      | Some res => Some (upd_regmap d (Val.longofintu res) rs, BPF_OK)
                      | None     => None
                      end
                    else
                      Some (rs, BPF_ILLEGAL_DIV)
      | BPF_XOR  => Some (upd_regmap d (Val.longofintu (Val.xor  d32 s32)) rs, BPF_OK)
      | BPF_MOV  => Some (upd_regmap d (Val.longofintu s32) rs, BPF_OK)
      | BPF_ARSH => if compu_lt s32 (Vint (Int.repr 32)) then
                      Some (upd_regmap d (Val.longofint (Val.shr  d32 s32)) rs, BPF_OK)
                    else
                      Some (rs, BPF_ILLEGAL_SHIFT)
      end
  | A64 =>
    let d64 := eval_regmap d rs in
    let s64 := eval_src s rs in
      match bop with
      | BPF_ADD  => Some (upd_regmap d (Val.addl  d64 s64) rs, BPF_OK)
      | BPF_SUB  => Some (upd_regmap d (Val.subl  d64 s64) rs, BPF_OK)
      | BPF_MUL  => Some (upd_regmap d (Val.mull  d64 s64) rs, BPF_OK)
      | BPF_DIV  => if compl_ne s64 val64_zero then
                      match Val.divlu d64 s64 with
                      | Some res => Some (upd_regmap d res rs, BPF_OK)
                      | None     => None
                      end
                    else
                      Some (rs, BPF_ILLEGAL_DIV)
      | BPF_OR   => Some (upd_regmap d (Val.orl   d64 s64) rs, BPF_OK)
      | BPF_AND  => Some (upd_regmap d (Val.andl  d64 s64) rs, BPF_OK)
      | BPF_LSH  => if complu_lt (val_intuoflongu s64) (Vint (Int.repr 64)) then
                      Some (upd_regmap d (Val.shll d64 (val_intuoflongu s64)) rs, BPF_OK)
                    else
                      Some (rs, BPF_ILLEGAL_SHIFT)
      | BPF_RSH  => if complu_lt (val_intuoflongu s64) (Vint (Int.repr 64)) then
                      Some (upd_regmap d (Val.shrlu d64 (val_intuoflongu s64)) rs, BPF_OK)
                    else
                      Some (rs, BPF_ILLEGAL_SHIFT)
      | BPF_MOD  => if compl_ne s64 val64_zero then
                      match Val.modlu d64 s64 with
                      | Some res => Some (upd_regmap d res rs, BPF_OK)
                      | None     => None
                      end
                    else
                      Some (rs, BPF_ILLEGAL_DIV)

      | BPF_XOR  => Some (upd_regmap d (Val.xorl  d64 s64) rs, BPF_OK)
      | BPF_MOV  => Some (upd_regmap d s64 rs, BPF_OK)
      | BPF_ARSH => if complu_lt (val_intuoflongu s64) (Vint (Int.repr 64)) then
                      Some (upd_regmap d (Val.shrl d64 (val_intuoflongu s64)) rs, BPF_OK)
                    else
                      Some (rs, BPF_ILLEGAL_SHIFT)
      end
  end.

Definition step_branch_cond (c: cond) (d: reg) (s: reg+imm) (rs: regmap): bool :=
  let dst := eval_regmap d rs in
  let src := eval_src s rs in
    match c with
    | Eq  => compl_eq   dst src
    | SEt => complu_set dst src
    | Ne  => compl_ne   dst src
    | Gt sign => 
      match sign with
      | Unsigned => complu_gt dst src
      | Signed   => compl_gt  dst src
      end
    | Ge sign =>
      match sign with
      | Unsigned => complu_ge dst src
      | Signed   => compl_ge  dst src
      end
    | Lt sign => 
      match sign with
      | Unsigned => complu_lt dst src
      | Signed   => compl_lt  dst src
      end
    | Le sign => 
      match sign with
      | Unsigned => complu_le dst src
      | Signed   => compl_le  dst src
      end
    end.

Definition check_one_memory_region (mr: memory_region) (perm: permission) (addr: val) (chunk: memory_chunk): val :=
  let lo_ofs := Val.sub addr (start_addr mr) in
  let hi_ofs := Val.add lo_ofs (memory_chunk_to_valu32 chunk) in
    if andb (andb
              (compu_le hi_ofs (block_size mr))
              (andb (compu_le lo_ofs (memory_chunk_to_valu32_upbound chunk))
                    (comp_eq Vzero (val32_modu lo_ofs (memory_chunk_to_valu32 chunk)))))
            (perm_ge (block_perm mr) perm) then
      Val.add (block_ptr mr) lo_ofs
    else
      Vnullptr.

Definition get_mem_region (n mrs_num: nat) (mrs: MyMemRegionsType): option memory_region :=
  if (Nat.ltb n mrs_num) then
    match List.nth_error mrs n with
    | Some mr => Some mr
    | None => None
    end
  else
    None.


(**r could be may abstract *)
Fixpoint check_all_memory_regions (pc: int) (num cache_id mrs_num: nat) (cache: list nat) (perm: permission)
  (chunk: memory_chunk) (addr: val) (mrs: MyMemRegionsType) (m: mem) (opt_flag: bool) {struct num}: option (val * list nat) :=
  match num with
  | O => Some (Vnullptr, cache)
  | S n =>
    match get_mem_region n mrs_num mrs with
    | None => None
    | Some cur_mr =>
      if opt_flag then
        if Nat.eqb num cache_id then (**r skip the unhint cache *)
          check_all_memory_regions pc n cache_id mrs_num cache perm chunk addr mrs m opt_flag
        else
          let check_ptr := check_one_memory_region cur_mr perm addr chunk in
            match cmp_ptr32_null m check_ptr with
            | None => None
            | Some is_null =>
              if is_null then
                check_all_memory_regions pc n cache_id mrs_num cache perm chunk addr mrs m opt_flag
              else
                (**r update cache with num; so cache_id can never be 0 *)
                match ListNat.assign cache (Z.to_nat (Int.unsigned pc)) num with
                | None => None
                | Some cache' => Some (check_ptr, cache')
                end
            end
      else
        let check_ptr  := check_one_memory_region cur_mr perm addr chunk in
          match cmp_ptr32_null m check_ptr with
          | Some is_null =>
            if is_null then
              check_all_memory_regions pc n cache_id mrs_num cache perm chunk addr mrs m opt_flag
            else
              Some (check_ptr, cache)
          | None => None
          end
    end
  end.

Definition check_mem (pc: int) (cache: list nat) (perm: permission) (chunk: memory_chunk) (addr: val) (opt_flag: bool)
  (mrs_num: nat) (mrs: MyMemRegionsType) (m: mem): option (val * list nat) :=
  if opt_flag then
    match List.nth_error cache (Z.to_nat (Int.unsigned pc)) with
    | None => None
    | Some cache_id =>
      if (Nat.eqb 0%nat cache_id) then (**r cache missing *)
        check_all_memory_regions pc mrs_num cache_id mrs_num cache perm chunk addr mrs m opt_flag
      else (**r cache_exist *)
        match get_mem_region (Nat.sub cache_id 1%nat) mrs_num mrs with
        | None => None
        | Some cur_mr =>
          let check_ptr := check_one_memory_region cur_mr perm addr chunk in
            match cmp_ptr32_null m check_ptr with
            | Some is_null =>
              if is_null then (**r cache missing *)
                check_all_memory_regions pc mrs_num cache_id mrs_num cache perm chunk addr mrs m opt_flag
              else
                Some (check_ptr, cache)
            | None => None
            end
        end
    end
  else
    check_all_memory_regions pc mrs_num 0%nat mrs_num cache perm chunk addr mrs m opt_flag.

Definition load_mem (chunk: memory_chunk) (ptr: val) (m: mem): option val :=
  match chunk with
  | Mint8unsigned | Mint16unsigned | Mint32 =>
    match Mem.loadv chunk m ptr with
    | Some Vundef => None
    | Some res => _to_vlong res
    | None => None
    end
  | Mint64 =>
    match Mem.loadv chunk m ptr with
    | Some Vundef => None
    | Some res => Some res
    | None => None
    end
  | _ => None
  end.

Definition step_load_x_operation (pc: int) (cache: list nat) (chunk: memory_chunk)
  (d:reg) (s:reg) (ofs:off) (rs: regmap) (mrs_num: nat) (mrs: MyMemRegionsType)
  (m: mem) (opt_flag: bool): option (regmap * bpf_flag * list nat) :=
  let sv    := eval_regmap s rs in
  let addr  := val_intuoflongu (Val.addl sv (Val.longofint (sint32_to_vint ofs))) in
    match check_mem pc cache Readable chunk addr opt_flag mrs_num mrs m with
    | None => None
    | Some (ptr, cache') =>
      match cmp_ptr32_null m ptr with
      | None => None
      | Some is_null =>
        if is_null then
          Some (rs, BPF_ILLEGAL_MEM, cache')
        else
          match load_mem chunk ptr m with
          | None => None
          | Some v =>
            let rs' := upd_regmap d v rs in
              Some (rs', BPF_OK, cache')
          end
      end
    end.

Definition store_mem_imm (ptr: val) (chunk: memory_chunk) (v: val) (m: mem): option mem :=
  match chunk with
  | Mint8unsigned | Mint16unsigned | Mint32 | Mint64 =>
    let src := vint_to_vint_or_vlong chunk v in
      Mem.storev chunk m ptr src
  | _ => None
  end.

Definition store_mem_reg (ptr: val) (chunk: memory_chunk) (v: val) (m: mem): option mem :=
  match chunk with
  | Mint8unsigned | Mint16unsigned | Mint32 | Mint64 =>
    let src := vlong_to_vint_or_vlong chunk v in
      Mem.storev chunk m ptr src
  | _ => None (*Some (upd_flag BPF_ILLEGAL_MEM st)*)
  end.

Definition step_store_operation (pc: int) (cache: list nat) (chunk: memory_chunk)
  (d: reg) (s: reg+imm) (ofs: off) (rs: regmap) (mrs_num: nat)
  (mrs: MyMemRegionsType) (m: mem) (opt_flag: bool): option (mem * bpf_flag * list nat) :=
  let dv   := eval_regmap d rs in
  let addr := val_intuoflongu (Val.addl dv (Val.longofint (sint32_to_vint ofs))) in
    match s with
    | inl r =>
      let src := eval_regmap r rs in
        match check_mem pc cache Writable chunk addr opt_flag mrs_num mrs m with
        | None => None
        | Some (ptr, cache') =>
          match cmp_ptr32_null m ptr with
          | None => None
          | Some is_null =>
            if is_null then
              Some (m, BPF_ILLEGAL_MEM, cache')
            else
              match store_mem_reg ptr chunk src m with
              | None => None
              | Some m' => Some (m', BPF_OK, cache')
              end
          end
        end
    | inr i =>
      match check_mem pc cache Writable chunk addr opt_flag mrs_num mrs m with
      | None => None
      | Some (ptr, cache') =>
        match cmp_ptr32_null m ptr with
        | None => None
        | Some is_null =>
          if is_null then
            Some (m, BPF_ILLEGAL_MEM, cache')
          else
            match store_mem_imm ptr chunk (sint32_to_vint i) m with
            | None => None
            | Some m' => Some (m', BPF_OK, cache')
            end
        end
      end
    end.

Definition eval_ins (pc: int)(l: list int64) (len: nat): option int64 :=
  if (Int.cmpu Clt pc (Int.repr (Z.of_nat len))) then
    List.nth_error l (Z.to_nat (Int.unsigned pc))
  else
    None.

Axiom _bpf_get_call : val -> val.

Axiom exec_function : val -> val.

Definition step (pc: int) (cache: list nat) (l: list int64) (len: nat)
  (rs: regmap) (mrs_num: nat) (mrs: MyMemRegionsType) (m: mem) (opt_flag: bool):
  option (int * regmap * mem * bpf_flag * list nat) :=
  match eval_ins pc l len with
  | None => None
  | Some ins64 =>
    match decode ins64 with
    | None => None
    | Some ins =>
      match ins with
      | BPF_NEG a d =>
        let dv := eval_regmap d rs in
          match a with
          | A32 => Some (pc, upd_regmap d (Val.longofintu (Val.neg (val_intuoflongu dv))) rs, m, BPF_OK, cache)
          | A64 => Some (pc, upd_regmap d (Val.negl dv) rs, m, BPF_OK, cache)
          end

      | BPF_BINARY a bop d s =>
        match step_alu_binary_operation a bop d s rs with
        | None => None
        | Some (rs', f) => Some (pc, rs', m, f, cache)
        end

      | BPF_JA ofs => Some (Int.add pc ofs, rs, m, BPF_OK, cache)
      | BPF_JUMP c d s ofs =>
        let cond := step_branch_cond c d s rs in
          if cond then
            Some (Int.add pc ofs, rs, m, BPF_OK, cache)
          else
            Some (pc, rs, m, BPF_OK, cache)

      | BPF_LDDW_low d i => Some (pc, upd_regmap d (Val.longofintu (sint32_to_vint i)) rs, m, BPF_OK, cache)
      | BPF_LDDW_high d i =>
        let d64 := eval_regmap d rs in
          Some (pc, upd_regmap d (Val.orl d64
            (Val.shll (Val.longofintu (sint32_to_vint i)) (sint32_to_vint (Int.repr 32)))) rs, m, BPF_OK, cache)
      | BPF_LDX chunk d s ofs =>
        match step_load_x_operation pc cache chunk d s ofs rs mrs_num mrs m opt_flag with
        | None => None
        | Some (rs', f, cache') => Some (pc, rs', m, f, cache')
        end
      | BPF_ST chunk d s ofs =>
        match step_store_operation pc cache chunk d s ofs rs mrs_num mrs m opt_flag with
        | None => None
        | Some (m, f, cache') => Some (pc, rs, m, f, cache')
        end

      | BPF_CALL i =>
        let f_ptr := _bpf_get_call (Vint ((Int.repr (Int64.unsigned (Int64.repr (Int.signed i)))))) in
          match cmp_ptr32_null m f_ptr with
          | None => None
          | Some is_null =>
            if is_null then
              Some (pc, rs, m, BPF_ILLEGAL_CALL, cache)
            else
              let res := exec_function f_ptr in
                Some (pc, upd_regmap R0 (Val.longofintu res) rs, m , BPF_OK, cache)
          end
      | BPF_RET    => Some (pc, rs, m, BPF_SUCC_RETURN, cache)
      | BPF_ERR    => Some (pc, rs, m, BPF_ILLEGAL_INSTRUCTION, cache)
      end
    end
  end.

Fixpoint bpf_interpreter_aux (fuel: nat) (pc: int) (cache: list nat) (l: list int64) (len: nat) (rs: regmap) (mrs_num: nat)
  (mrs: MyMemRegionsType) (m: mem) (opt_flag: bool) {struct fuel}: option (int * regmap * mem * bpf_flag * list nat) :=
  match fuel with
  | O => Some (pc, rs, m, BPF_ILLEGAL_LEN, cache)
  | S fuel0 =>
    if Int.cmpu Clt pc (Int.repr (Z.of_nat len)) then
      match step pc cache l len rs mrs_num mrs m opt_flag with
      | None => None
      | Some (pc', rs', m', f', cache') =>
        if flag_eq f' BPF_OK then
          if Int.cmpu Clt (Int.add pc' Int.one) (Int.repr (Z.of_nat len)) then
            bpf_interpreter_aux fuel0 (Int.add pc' Int.one) cache' l len rs' mrs_num mrs m' opt_flag
          else
            Some (pc', rs', m', BPF_ILLEGAL_LEN, cache')
        else
          Some (pc', rs', m', f', cache')
      end
    else
      Some (pc, rs, m, BPF_ILLEGAL_LEN, cache)
  end.

Definition bpf_interpreter (fuel: nat) (ctx_ptr: val) (pc: int) (cache: list nat) (l: list int64) (len: nat)
  (rs: regmap) (mrs_num: nat) (mrs: MyMemRegionsType) (m: mem) (opt_flag: bool): option (val * int * regmap * mem * bpf_flag * list nat)  :=
  let rs1 := upd_regmap R1 (Val.longofintu ctx_ptr) rs in
    match bpf_interpreter_aux fuel pc cache l len rs1 mrs_num mrs m opt_flag with
    | None => None
    | Some (pc', rs', m', f', cache') =>
      if flag_eq f' BPF_SUCC_RETURN then
        Some (eval_regmap R0 rs', pc', rs', m', f', cache')
      else
        Some (val64_zero, pc', rs', m', f', cache')
    end.